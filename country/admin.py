from django.contrib import admin
from .models import Country, Department, Municipality


# Register your models here.
class CountryAdmin(admin.ModelAdmin):
    readonly_fields = ('created',)


class DepartmentAdmin(admin.ModelAdmin):
    readonly_fields = ('created',)


class MunicipalityAdmin(admin.ModelAdmin):
    readonly_fields = ('created',)


# admin.site.register(Country, CountryAdmin)
# admin.site.register(Department, DepartmentAdmin)
# admin.site.register(Municipality, MunicipalityAdmin)