from django.db import models


# Create your models here.
class Country(models.Model):
    name = models.CharField(max_length=100, verbose_name='Nombre')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')

    class Meta:
        verbose_name = 'país'
        verbose_name_plural = 'paises'

    def __str__(self):
        return self.name


class Department(models.Model):
    name = models.CharField(max_length=100, verbose_name='Nombre')
    country = models.ForeignKey(Country, verbose_name= 'País', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fehca de creación')

    class Meta:
        verbose_name = 'departamento / estado'
        verbose_name_plural = 'departamentos / estados'

    def __str__(self):
        return self.name


class Municipality(models.Model):
    name = models.CharField(max_length=100, verbose_name='Nombre')
    department = models.ForeignKey(Department, verbose_name='Departamento', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')

    class Meta:
        verbose_name = 'municipio'
        verbose_name_plural = 'municipios'

    def __str__(self):
        return self.name
