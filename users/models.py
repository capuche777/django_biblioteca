from django.db import models
from django.contrib.auth.models import User
from country.models import Department, Municipality


# Create your models here.
class AdminExtra(models.Model):
    user = models.ForeignKey(User, verbose_name= 'Usuario')
    address = models.TextField(verbose_name= 'Direccion')
    phone = models.CharField(max_length=8, verbose_name= 'Teléfono')
    birth = models.DateField(verbose_name= 'Fecha de nacimiento')
    cui = models.CharField(max_length=13, verbose_name= 'CUI')
    department = models.ForeignKey(Department, verbose_name= 'Departamento')
    municipality = models.ForeignKey(Municipality, verbose_name= 'Municipio')

    