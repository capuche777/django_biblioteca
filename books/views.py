from django.shortcuts import render, get_object_or_404
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.urls import reverse, reverse_lazy
# from django.shortcuts import redirect
from .models import Theme, Author, Title
from .forms import ThemeForm, AuthorForm, TitleForm


# Classes for list, create, edit and delete views in themes
@method_decorator(login_required, name='dispatch')
class ThemeListView(ListView):
    model = Theme


@method_decorator(staff_member_required, name='dispatch')
class ThemeCreate(CreateView):
    model = Theme
    form_class = ThemeForm
    success_url = reverse_lazy('books:temas')


@method_decorator(staff_member_required, name='dispatch')
class ThemeUpdate(UpdateView):
    model = Theme
    form_class = ThemeForm
    template_name_suffix = '_update_form'

    def get_success_url(self):
        return reverse_lazy('books:actualizar-tema', args=[self.object.id]) + '?ok'


@method_decorator(staff_member_required, name='dispatch')
class ThemeDelete(DeleteView):
    model = Theme
    success_url = reverse_lazy('books:temas')


@method_decorator(login_required, name='dispatch')
class AuthorListView(ListView):
    model = Author


@method_decorator(staff_member_required, name='dispatch')
class AuthorCreate(CreateView):
    model = Author
    form_class = AuthorForm
    success_url = reverse_lazy('books:autores')


@method_decorator(staff_member_required, name='dispatch')
class AuthorUpdate(UpdateView):
    model = Author
    form_class = AuthorForm
    template_name_suffix = '_update_form'

    def get_success_url(self):
        return reverse_lazy('books:actualizar-autor', args=[self.object.id]) + '?ok'


@method_decorator(staff_member_required, name='dispatch')
class AuthorDelete(DeleteView):
    model = Author
    success_url = reverse_lazy('books:autores')


@method_decorator(login_required, name='dispatch')
class BookListView(ListView):
    model = Title


# Classes for detailed view of the book
@method_decorator(login_required, name='dispatch')
class BookDetailView(DetailView):
    model = Title


@method_decorator(staff_member_required, name='dispatch')
class BookUpdate(UpdateView):
    model = Title
    form_class = TitleForm
    template_name_suffix = '_update_form'

    def get_success_url(self):
        return reverse_lazy('books:actualizar-libro', args=[self.object.id]) + '?ok'


@method_decorator(staff_member_required, name='dispatch')
class BookDelete(DeleteView):
    model = Title
    success_url = reverse_lazy('books:libros')


@method_decorator(staff_member_required, name='dispatch')
class BookCreate(CreateView):
    model = Title
    form_class = TitleForm
    success_url = reverse_lazy('books:libros')


# Functions to take an specific theme or author view
@login_required()
def book_theme(request, theme_id):
    theme = get_object_or_404(Theme, id = theme_id)
    return render(request, 'books/libros-tema.html', { 'theme': theme })


@login_required()
def book_author(request, author_id):
    author = get_object_or_404(Author, id = author_id)
    return render(request, 'books/libros-autor.html', { 'author': author })
