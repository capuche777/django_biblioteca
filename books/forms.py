from django import forms
from .models import Theme, Author, Title


class ThemeForm(forms.ModelForm):
    class Meta:
        model = Theme
        fields = ['name']
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'class': 'form-control input-info',
                    'placeholder': 'Ingrese tema',
                    'autofocus': ''
                }
            )
        }
        labels = {
            'name': ''
        }

class AuthorForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = ['name', 'surname', 'country', 'gender', 'birth', 'death']


class TitleForm(forms.ModelForm):
    class Meta:
        model = Title
        fields = ['title', 'author', 'topic', 'location', 'availability']