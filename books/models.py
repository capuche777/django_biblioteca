from django.db import models
from core.models import Genre
from country.models import Country


# Create your models here.
class Theme(models.Model):
    name = models.CharField(max_length=100, unique=True,verbose_name='Tema')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de ingreso')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de Actualización')

    class Meta:
        verbose_name= 'tema'
        verbose_name_plural= 'temas'
        ordering= ['-created']

    def __str__(self):
        return self.name


class Author(models.Model):
    name = models.CharField(max_length=100, verbose_name= 'Nombres')
    surname = models.CharField(max_length=100, verbose_name= 'Apellidos')
    country = models.ForeignKey(Country, verbose_name= 'Pais', on_delete=models.CASCADE)
    gender = models.ForeignKey(Genre, verbose_name='Genero', on_delete=models.CASCADE)
    birth = models.DateField(verbose_name= 'Nacimiento')
    death = models.DateField(verbose_name= 'Fallecimiento', null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de ingreso')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de Actualización')

    class Meta:
        verbose_name = 'autor'
        verbose_name_plural = 'autores'
        ordering = ['-created']

    def __str__(self):
        return f'{self.name} {self.surname}'

    @property
    def books(self):
        return len(Title.objects.filter(author__id=self.id))


class Title(models.Model):
    title = models.CharField(max_length=200, verbose_name='Libro')
    author = models.ForeignKey(Author, verbose_name= 'Autor', on_delete=models.CASCADE, related_name='get_authors')
    topic = models.ForeignKey(Theme, verbose_name= 'Tema', on_delete=models.CASCADE, related_name='get_themes')
    location = models.CharField(max_length=200, verbose_name= 'Ubicación')
    availability = models.IntegerField(verbose_name= 'Disponibilidad')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de ingreso')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de Actualización')

    class Meta:
        verbose_name = 'libro'
        verbose_name_plural = 'libros'
        ordering = ['-created']

    def __str__(self):
        return self.title
