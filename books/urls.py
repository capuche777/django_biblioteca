from django.urls import path
from . import views
from .views import ThemeListView, ThemeCreate, ThemeUpdate, ThemeDelete, BookListView, BookCreate, BookUpdate, BookDelete, AuthorListView, AuthorCreate, AuthorUpdate, BookDetailView, AuthorDelete

books_patterns = ([
    # Themes urls
    path('temas/create/', ThemeCreate.as_view(), name='crear-tema'),
    path('temas/', ThemeListView.as_view(), name='temas'),
    path('temas/<int:theme_id>', views.book_theme, name= 'tema'),
    path('temas/update/<int:pk>', ThemeUpdate.as_view(), name='actualizar-tema'),
    path('temas/delete/<int:pk>', ThemeDelete.as_view(), name='eliminar-tema'),
    # Author urls
    path('autores/create', AuthorCreate.as_view(), name='crear-autor'),
    path('autores/', AuthorListView.as_view(), name='autores'),
    path('autores/<int:author_id>', views.book_author, name= 'autor'),
    path('autores/update/<int:pk>', AuthorUpdate.as_view(), name='actualizar-autor'),
    path('autores/delete/<int:pk>', AuthorDelete.as_view(), name='eliminar-autor'),
    #books urls
    path('libros/create', BookCreate.as_view(), name='crear-libro'),
    path('libros/', BookListView.as_view(), name='libros'),
    path('libros/<int:pk>/<slug:slug>', BookDetailView.as_view(), name = 'prestar'),
    path('libros/update/<int:pk>', BookUpdate.as_view(), name='actualizar-libro'),
    path('libros/delete/<int:pk>', BookDelete.as_view(), name='eliminar-libro'),
], 'books')