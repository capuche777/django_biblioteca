from django.contrib import admin
from .models import Theme, Author, Title


# Register your models here.
class ThemeAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')
    list_display = ('name', 'created')


class AuthorAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')
    list_display = ('__str__', 'country', 'created')
    search_fields = ('name', 'surname', 'country__name')
    date_hierarchy = 'created'


class TitleAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')
    list_display = ('title', 'author', 'topic', 'location', 'availability')
    search_fields = ('title', 'author__name', 'topic__name', 'location', 'availability')
    date_hierarchy = 'created'


admin.site.register(Theme, ThemeAdmin)
admin.site.register(Author, AuthorAdmin)
admin.site.register(Title, TitleAdmin)