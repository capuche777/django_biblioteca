from django.urls import path
from . import views

urlpatterns = [
    path('temas/', views.admin_temas, name='admin-temas'),
    path('autores/', views.admin_autores, name = 'admin-autores'),
    path('libros/', views.admin_libros, name = 'admin-libros'),
]