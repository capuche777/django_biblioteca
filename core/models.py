from django.db import models

# Create your models here.
class Genre(models.Model):
    genre = models.CharField(max_length=10, unique=True,verbose_name='Genero')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de ingreso')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de Actualización')

    class Meta:
        verbose_name = 'genero'
        verbose_name_plural = 'generos'
        ordering = ['-created']

    def __str__(self):
        return self.genre

class Lend_State(models.Model):
    state = models.CharField(max_length=20, unique=True, verbose_name='Estado')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de ingreso')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de Actualización')

    class Meta:
        verbose_name = 'estado'
        verbose_name_plural = 'estados'
        ordering = ['-created']

    def __str__(self):
        return self.state