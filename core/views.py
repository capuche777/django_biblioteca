from django.shortcuts import render, HttpResponse


# Create your views here.
def admin_temas(request):
    return render(request, 'core/temas-mostrar.html')


def admin_autores(request):
    return HttpResponse('<h1>Autores</h1>')


def admin_libros(request):
    return HttpResponse('<h1>Libros</h1>')