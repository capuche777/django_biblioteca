from django.contrib import admin
from .models import Genre, Lend_State


# Register your models here.
class GenreAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')


class Lend_StateAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')

# admin.site.register(Genre, GenreAdmin)
# admin.site.register(Lend_State, Lend_StateAdmin)
