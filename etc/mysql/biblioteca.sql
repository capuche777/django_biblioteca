-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-07-2018 a las 00:26:05
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `biblioteca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can add permission', 2, 'add_permission'),
(5, 'Can change permission', 2, 'change_permission'),
(6, 'Can delete permission', 2, 'delete_permission'),
(7, 'Can add group', 3, 'add_group'),
(8, 'Can change group', 3, 'change_group'),
(9, 'Can delete group', 3, 'delete_group'),
(10, 'Can add user', 4, 'add_user'),
(11, 'Can change user', 4, 'change_user'),
(12, 'Can delete user', 4, 'delete_user'),
(13, 'Can add content type', 5, 'add_contenttype'),
(14, 'Can change content type', 5, 'change_contenttype'),
(15, 'Can delete content type', 5, 'delete_contenttype'),
(16, 'Can add session', 6, 'add_session'),
(17, 'Can change session', 6, 'change_session'),
(18, 'Can delete session', 6, 'delete_session'),
(19, 'Can add departamento / estado', 7, 'add_department'),
(20, 'Can change departamento / estado', 7, 'change_department'),
(21, 'Can delete departamento / estado', 7, 'delete_department'),
(22, 'Can add municipio', 8, 'add_municipality'),
(23, 'Can change municipio', 8, 'change_municipality'),
(24, 'Can delete municipio', 8, 'delete_municipality'),
(25, 'Can add país', 9, 'add_country'),
(26, 'Can change país', 9, 'change_country'),
(27, 'Can delete país', 9, 'delete_country'),
(28, 'Can add libro', 10, 'add_title'),
(29, 'Can change libro', 10, 'change_title'),
(30, 'Can delete libro', 10, 'delete_title'),
(31, 'Can add autor', 11, 'add_author'),
(32, 'Can change autor', 11, 'change_author'),
(33, 'Can delete autor', 11, 'delete_author'),
(34, 'Can add tema', 12, 'add_theme'),
(35, 'Can change tema', 12, 'change_theme'),
(36, 'Can delete tema', 12, 'delete_theme'),
(37, 'Can add genero', 13, 'add_genre'),
(38, 'Can change genero', 13, 'change_genre'),
(39, 'Can delete genero', 13, 'delete_genre'),
(40, 'Can add genero', 14, 'add_genre'),
(41, 'Can change genero', 14, 'change_genre'),
(42, 'Can delete genero', 14, 'delete_genre'),
(43, 'Can add estado', 15, 'add_lend_state'),
(44, 'Can change estado', 15, 'change_lend_state'),
(45, 'Can delete estado', 15, 'delete_lend_state'),
(46, 'Can add prestamo', 16, 'add_lend'),
(47, 'Can change prestamo', 16, 'change_lend'),
(48, 'Can delete prestamo', 16, 'delete_lend');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$100000$EA1FvmvMHg8C$mCpLaZ5I18Cr1jfLk6PXuWjgYLkZUJJqyAwhF5SjMQM=', '2018-07-24 21:50:30.841623', 1, 'capuche777', 'Jeremias', 'Enriquez', 'capuche777@gmail.com', 1, 1, '2018-06-30 21:31:29.000000'),
(2, 'pbkdf2_sha256$100000$M4bpOx3hSA1A$3zt2BujSkFdIC0vs/gItyKAsOIZMO/GiFqZ8lu3q6HE=', '2018-07-24 22:00:47.520327', 0, 'cloarca', 'Carlos', 'Loarca', 'nose@nose.com', 0, 1, '2018-07-12 21:54:41.000000'),
(3, 'pbkdf2_sha256$100000$UgDVmg5042Dy$hVkGZD0FL8tbqpPKck90wKKNlwtn5ybDYLqpLFX2/z0=', '2018-07-19 16:04:11.830546', 1, 'jeremiasenriquez', '', '', 'capuche777@hotmail.com', 1, 1, '2018-07-19 16:04:01.604853'),
(4, 'pbkdf2_sha256$100000$KzO6VYI1mmHp$dwv2wCSVFAvlAh2LydaO2UfZPKNtzJ4Ao6VBWaIu1JQ=', '2018-07-19 16:56:17.314667', 1, 'otronumerouno', 'Julio', 'Sinay', 'cualquiercorreo@otro.cm', 1, 1, '2018-07-19 16:54:07.000000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `books_author`
--

CREATE TABLE `books_author` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `birth` date NOT NULL,
  `death` date DEFAULT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `country_id` int(11) NOT NULL,
  `gender_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `books_author`
--

INSERT INTO `books_author` (`id`, `name`, `surname`, `birth`, `death`, `created`, `updated`, `country_id`, `gender_id`) VALUES
(5, 'J. K.', 'Rowling', '1965-07-31', NULL, '2018-07-04 17:52:32.636028', '2018-07-04 17:52:32.636028', 142, 1),
(6, 'Gustavo', 'Seijas', '1980-07-20', '2018-07-19', '2018-07-19 16:57:58.966593', '2018-07-19 16:58:21.091083', 43, 2),
(7, 'Carmelo', 'Polla', '1899-10-19', '1974-06-09', '2018-07-23 21:11:29.222062', '2018-07-23 21:11:29.222062', 3, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `books_theme`
--

CREATE TABLE `books_theme` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `books_theme`
--

INSERT INTO `books_theme` (`id`, `name`, `created`, `updated`) VALUES
(2, 'Novela', '2018-07-02 15:15:50.155065', '2018-07-02 15:15:50.155065'),
(3, 'Otro', '2018-07-02 22:30:20.954729', '2018-07-02 22:30:20.954729'),
(4, 'Deportes', '2018-07-04 17:20:01.828764', '2018-07-04 17:20:01.829747'),
(5, 'Economia', '2018-07-04 17:20:20.609905', '2018-07-04 17:20:20.609905'),
(6, 'Escolar', '2018-07-19 16:58:57.730675', '2018-07-19 16:58:57.730675'),
(7, 'Inspiración', '2018-07-21 04:41:20.620188', '2018-07-21 04:41:20.620188'),
(8, 'Nopor', '2018-07-21 04:46:15.290274', '2018-07-21 04:46:15.290274'),
(10, 'Black and Yellow', '2018-07-21 04:47:40.078208', '2018-07-21 04:47:40.078208'),
(11, 'Informática', '2018-07-21 05:27:16.388486', '2018-07-21 15:35:41.966828');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `books_title`
--

CREATE TABLE `books_title` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `location` varchar(200) NOT NULL,
  `availability` int(11) NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `author_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `books_title`
--

INSERT INTO `books_title` (`id`, `title`, `location`, `availability`, `created`, `updated`, `author_id`, `topic_id`) VALUES
(4, 'Harry Potter', 'Novela-Estante 5, Fila', 50, '2018-07-04 17:53:16.689156', '2018-07-04 18:05:49.963375', 5, 2),
(7, 'Diseño sobre la optimización de los envases plasticos', 'Algun Lugar', 200, '2018-07-19 16:59:47.127918', '2018-07-19 16:59:47.127918', 6, 6),
(8, 'noticia nueva', 'Algun Lugar', 10, '2018-07-19 17:02:52.573989', '2018-07-19 17:02:52.573989', 6, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `core_genre`
--

CREATE TABLE `core_genre` (
  `id` int(11) NOT NULL,
  `genre` varchar(10) NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `core_genre`
--

INSERT INTO `core_genre` (`id`, `genre`, `created`, `updated`) VALUES
(1, 'Femenino', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000'),
(2, 'Masculino', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `core_lend_state`
--

CREATE TABLE `core_lend_state` (
  `id` int(11) NOT NULL,
  `state` varchar(20) NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `core_lend_state`
--

INSERT INTO `core_lend_state` (`id`, `state`, `created`, `updated`) VALUES
(1, 'Prestado', '2018-07-12 17:40:35.203340', '2018-07-12 17:40:35.203340'),
(2, 'Mora', '2018-07-12 17:40:42.437846', '2018-07-12 17:40:42.437846'),
(3, 'Devuelto', '2018-07-12 17:40:51.170617', '2018-07-12 17:40:51.170617'),
(4, 'Devuelto con mora', '2018-07-12 17:40:58.816391', '2018-07-12 17:40:58.816391');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `country_country`
--

CREATE TABLE `country_country` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `country_country`
--

INSERT INTO `country_country` (`id`, `name`, `created`) VALUES
(1, 'Afganistán\r', '0000-00-00 00:00:00.000000'),
(2, 'Albania\r', '0000-00-00 00:00:00.000000'),
(3, 'Alemania\r', '0000-00-00 00:00:00.000000'),
(4, 'Andorra\r', '0000-00-00 00:00:00.000000'),
(5, 'Angola\r', '0000-00-00 00:00:00.000000'),
(6, 'Antigua y Barbuda\r', '0000-00-00 00:00:00.000000'),
(7, 'Arabia Saudita\r', '0000-00-00 00:00:00.000000'),
(8, 'Argelia\r', '0000-00-00 00:00:00.000000'),
(9, 'Argentina\r', '0000-00-00 00:00:00.000000'),
(10, 'Armenia\r', '0000-00-00 00:00:00.000000'),
(11, 'Australia\r', '0000-00-00 00:00:00.000000'),
(12, 'Austria\r', '0000-00-00 00:00:00.000000'),
(13, 'Azerbaiyán\r', '0000-00-00 00:00:00.000000'),
(14, 'Bahamas\r', '0000-00-00 00:00:00.000000'),
(15, 'Bangladés\r', '0000-00-00 00:00:00.000000'),
(16, 'Barbados\r', '0000-00-00 00:00:00.000000'),
(17, 'Baréin\r', '0000-00-00 00:00:00.000000'),
(18, 'Bélgica\r', '0000-00-00 00:00:00.000000'),
(19, 'Belice\r', '0000-00-00 00:00:00.000000'),
(20, 'Benín\r', '0000-00-00 00:00:00.000000'),
(21, 'Bielorrusia\r', '0000-00-00 00:00:00.000000'),
(22, 'Birmania\r', '0000-00-00 00:00:00.000000'),
(23, 'Bolivia\r', '0000-00-00 00:00:00.000000'),
(24, 'Bosnia y Herzegovina\r', '0000-00-00 00:00:00.000000'),
(25, 'Botsuana\r', '0000-00-00 00:00:00.000000'),
(26, 'Brasil\r', '0000-00-00 00:00:00.000000'),
(27, 'Brunéi\r', '0000-00-00 00:00:00.000000'),
(28, 'Bulgaria\r', '0000-00-00 00:00:00.000000'),
(29, 'Burkina Faso\r', '0000-00-00 00:00:00.000000'),
(30, 'Burundi\r', '0000-00-00 00:00:00.000000'),
(31, 'Bután\r', '0000-00-00 00:00:00.000000'),
(32, 'Cabo Verde\r', '0000-00-00 00:00:00.000000'),
(33, 'Camboya\r', '0000-00-00 00:00:00.000000'),
(34, 'Camerún\r', '0000-00-00 00:00:00.000000'),
(35, 'Canadá\r', '0000-00-00 00:00:00.000000'),
(36, 'Catar\r', '0000-00-00 00:00:00.000000'),
(37, 'Chad\r', '0000-00-00 00:00:00.000000'),
(38, 'Chile\r', '0000-00-00 00:00:00.000000'),
(39, 'China\r', '0000-00-00 00:00:00.000000'),
(40, 'Chipre\r', '0000-00-00 00:00:00.000000'),
(41, 'Ciudad del Vaticano\r', '0000-00-00 00:00:00.000000'),
(42, 'Colombia\r', '0000-00-00 00:00:00.000000'),
(43, 'Comoras\r', '0000-00-00 00:00:00.000000'),
(44, 'Corea del Norte\r', '0000-00-00 00:00:00.000000'),
(45, 'Corea del Sur\r', '0000-00-00 00:00:00.000000'),
(46, 'Costa de Marfil\r', '0000-00-00 00:00:00.000000'),
(47, 'Costa Rica\r', '0000-00-00 00:00:00.000000'),
(48, 'Croacia\r', '0000-00-00 00:00:00.000000'),
(49, 'Cuba\r', '0000-00-00 00:00:00.000000'),
(50, 'Dinamarca\r', '0000-00-00 00:00:00.000000'),
(51, 'Dominica\r', '0000-00-00 00:00:00.000000'),
(52, 'Ecuador\r', '0000-00-00 00:00:00.000000'),
(53, 'Egipto\r', '0000-00-00 00:00:00.000000'),
(54, 'El Salvador\r', '0000-00-00 00:00:00.000000'),
(55, 'Emiratos Árabes Unidos\r', '0000-00-00 00:00:00.000000'),
(56, 'Eritrea\r', '0000-00-00 00:00:00.000000'),
(57, 'Eslovaquia\r', '0000-00-00 00:00:00.000000'),
(58, 'Eslovenia\r', '0000-00-00 00:00:00.000000'),
(59, 'España\r', '0000-00-00 00:00:00.000000'),
(60, 'Estados Unidos\r', '0000-00-00 00:00:00.000000'),
(61, 'Estonia\r', '0000-00-00 00:00:00.000000'),
(62, 'Etiopía\r', '0000-00-00 00:00:00.000000'),
(63, 'Filipinas\r', '0000-00-00 00:00:00.000000'),
(64, 'Finlandia\r', '0000-00-00 00:00:00.000000'),
(65, 'Fiyi\r', '0000-00-00 00:00:00.000000'),
(66, 'Francia\r', '0000-00-00 00:00:00.000000'),
(67, 'Gabón\r', '0000-00-00 00:00:00.000000'),
(68, 'Gambia\r', '0000-00-00 00:00:00.000000'),
(69, 'Georgia\r', '0000-00-00 00:00:00.000000'),
(70, 'Ghana\r', '0000-00-00 00:00:00.000000'),
(71, 'Granada\r', '0000-00-00 00:00:00.000000'),
(72, 'Grecia\r', '0000-00-00 00:00:00.000000'),
(73, 'Guatemala\r', '0000-00-00 00:00:00.000000'),
(74, 'Guyana\r', '0000-00-00 00:00:00.000000'),
(75, 'Guinea\r', '0000-00-00 00:00:00.000000'),
(76, 'Guinea ecuatorial\r', '0000-00-00 00:00:00.000000'),
(77, 'Guinea-Bisáu\r', '0000-00-00 00:00:00.000000'),
(78, 'Haití\r', '0000-00-00 00:00:00.000000'),
(79, 'Honduras\r', '0000-00-00 00:00:00.000000'),
(80, 'Hungría\r', '0000-00-00 00:00:00.000000'),
(81, 'India\r', '0000-00-00 00:00:00.000000'),
(82, 'Indonesia\r', '0000-00-00 00:00:00.000000'),
(83, 'Irak\r', '0000-00-00 00:00:00.000000'),
(84, 'Irán\r', '0000-00-00 00:00:00.000000'),
(85, 'Irlanda\r', '0000-00-00 00:00:00.000000'),
(86, 'Islandia\r', '0000-00-00 00:00:00.000000'),
(87, 'Islas Marshall\r', '0000-00-00 00:00:00.000000'),
(88, 'Islas Salomón\r', '0000-00-00 00:00:00.000000'),
(89, 'Israel\r', '0000-00-00 00:00:00.000000'),
(90, 'Italia\r', '0000-00-00 00:00:00.000000'),
(91, 'Jamaica\r', '0000-00-00 00:00:00.000000'),
(92, 'Japón\r', '0000-00-00 00:00:00.000000'),
(93, 'Jordania\r', '0000-00-00 00:00:00.000000'),
(94, 'Kazajistán\r', '0000-00-00 00:00:00.000000'),
(95, 'Kenia\r', '0000-00-00 00:00:00.000000'),
(96, 'Kirguistán\r', '0000-00-00 00:00:00.000000'),
(97, 'Kiribati\r', '0000-00-00 00:00:00.000000'),
(98, 'Kuwait\r', '0000-00-00 00:00:00.000000'),
(99, 'Laos\r', '0000-00-00 00:00:00.000000'),
(100, 'Lesoto\r', '0000-00-00 00:00:00.000000'),
(101, 'Letonia\r', '0000-00-00 00:00:00.000000'),
(102, 'Líbano\r', '0000-00-00 00:00:00.000000'),
(103, 'Liberia\r', '0000-00-00 00:00:00.000000'),
(104, 'Libia\r', '0000-00-00 00:00:00.000000'),
(105, 'Liechtenstein\r', '0000-00-00 00:00:00.000000'),
(106, 'Lituania\r', '0000-00-00 00:00:00.000000'),
(107, 'Luxemburgo\r', '0000-00-00 00:00:00.000000'),
(108, 'Madagascar\r', '0000-00-00 00:00:00.000000'),
(109, 'Malasia\r', '0000-00-00 00:00:00.000000'),
(110, 'Malaui\r', '0000-00-00 00:00:00.000000'),
(111, 'Maldivas\r', '0000-00-00 00:00:00.000000'),
(112, 'Malí\r', '0000-00-00 00:00:00.000000'),
(113, 'Malta\r', '0000-00-00 00:00:00.000000'),
(114, 'Marruecos\r', '0000-00-00 00:00:00.000000'),
(115, 'Mauricio\r', '0000-00-00 00:00:00.000000'),
(116, 'Mauritania\r', '0000-00-00 00:00:00.000000'),
(117, 'México\r', '0000-00-00 00:00:00.000000'),
(118, 'Micronesia\r', '0000-00-00 00:00:00.000000'),
(119, 'Moldavia\r', '0000-00-00 00:00:00.000000'),
(120, 'Mónaco\r', '0000-00-00 00:00:00.000000'),
(121, 'Mongolia\r', '0000-00-00 00:00:00.000000'),
(122, 'Montenegro\r', '0000-00-00 00:00:00.000000'),
(123, 'Mozambique\r', '0000-00-00 00:00:00.000000'),
(124, 'Namibia\r', '0000-00-00 00:00:00.000000'),
(125, 'Nauru\r', '0000-00-00 00:00:00.000000'),
(126, 'Nepal\r', '0000-00-00 00:00:00.000000'),
(127, 'Nicaragua\r', '0000-00-00 00:00:00.000000'),
(128, 'Níger\r', '0000-00-00 00:00:00.000000'),
(129, 'Nigeria\r', '0000-00-00 00:00:00.000000'),
(130, 'Noruega\r', '0000-00-00 00:00:00.000000'),
(131, 'Nueva Zelanda\r', '0000-00-00 00:00:00.000000'),
(132, 'Omán\r', '0000-00-00 00:00:00.000000'),
(133, 'Países Bajos\r', '0000-00-00 00:00:00.000000'),
(134, 'Pakistán\r', '0000-00-00 00:00:00.000000'),
(135, 'Palaos\r', '0000-00-00 00:00:00.000000'),
(136, 'Panamá\r', '0000-00-00 00:00:00.000000'),
(137, 'Papúa Nueva Guinea\r', '0000-00-00 00:00:00.000000'),
(138, 'Paraguay\r', '0000-00-00 00:00:00.000000'),
(139, 'Perú\r', '0000-00-00 00:00:00.000000'),
(140, 'Polonia\r', '0000-00-00 00:00:00.000000'),
(141, 'Portugal\r', '0000-00-00 00:00:00.000000'),
(142, 'Reino Unido\r', '0000-00-00 00:00:00.000000'),
(143, 'República Centroafricana\r', '0000-00-00 00:00:00.000000'),
(144, 'República Checa\r', '0000-00-00 00:00:00.000000'),
(145, 'República de Macedonia\r', '0000-00-00 00:00:00.000000'),
(146, 'República del Congo\r', '0000-00-00 00:00:00.000000'),
(147, 'República Democrática del Congo\r', '0000-00-00 00:00:00.000000'),
(148, 'República Dominicana\r', '0000-00-00 00:00:00.000000'),
(149, 'República Sudafricana\r', '0000-00-00 00:00:00.000000'),
(150, 'Ruanda\r', '0000-00-00 00:00:00.000000'),
(151, 'Rumanía\r', '0000-00-00 00:00:00.000000'),
(152, 'Rusia\r', '0000-00-00 00:00:00.000000'),
(153, 'Samoa\r', '0000-00-00 00:00:00.000000'),
(154, 'San Cristóbal y Nieves\r', '0000-00-00 00:00:00.000000'),
(155, 'San Marino\r', '0000-00-00 00:00:00.000000'),
(156, 'San Vicente y las Granadinas\r', '0000-00-00 00:00:00.000000'),
(157, 'Santa Lucía\r', '0000-00-00 00:00:00.000000'),
(158, 'Santo Tomé y Príncipe\r', '0000-00-00 00:00:00.000000'),
(159, 'Senegal\r', '0000-00-00 00:00:00.000000'),
(160, 'Serbia\r', '0000-00-00 00:00:00.000000'),
(161, 'Seychelles\r', '0000-00-00 00:00:00.000000'),
(162, 'Sierra Leona\r', '0000-00-00 00:00:00.000000'),
(163, 'Singapur\r', '0000-00-00 00:00:00.000000'),
(164, 'Siria\r', '0000-00-00 00:00:00.000000'),
(165, 'Somalia\r', '0000-00-00 00:00:00.000000'),
(166, 'Sri Lanka\r', '0000-00-00 00:00:00.000000'),
(167, 'Suazilandia\r', '0000-00-00 00:00:00.000000'),
(168, 'Sudán\r', '0000-00-00 00:00:00.000000'),
(169, 'Sudán del Sur\r', '0000-00-00 00:00:00.000000'),
(170, 'Suecia\r', '0000-00-00 00:00:00.000000'),
(171, 'Suiza\r', '0000-00-00 00:00:00.000000'),
(172, 'Surinam\r', '0000-00-00 00:00:00.000000'),
(173, 'Tailandia\r', '0000-00-00 00:00:00.000000'),
(174, 'Tanzania\r', '0000-00-00 00:00:00.000000'),
(175, 'Tayikistán\r', '0000-00-00 00:00:00.000000'),
(176, 'Timor Oriental\r', '0000-00-00 00:00:00.000000'),
(177, 'Togo\r', '0000-00-00 00:00:00.000000'),
(178, 'Tonga\r', '0000-00-00 00:00:00.000000'),
(179, 'Trinidad y Tobago\r', '0000-00-00 00:00:00.000000'),
(180, 'Túnez\r', '0000-00-00 00:00:00.000000'),
(181, 'Turkmenistán\r', '0000-00-00 00:00:00.000000'),
(182, 'Turquía\r', '0000-00-00 00:00:00.000000'),
(183, 'Tuvalu\r', '0000-00-00 00:00:00.000000'),
(184, 'Ucrania\r', '0000-00-00 00:00:00.000000'),
(185, 'Uganda\r', '0000-00-00 00:00:00.000000'),
(186, 'Uruguay\r', '0000-00-00 00:00:00.000000'),
(187, 'Uzbekistán\r', '0000-00-00 00:00:00.000000'),
(188, 'Vanuatu\r', '0000-00-00 00:00:00.000000'),
(189, 'Venezuela\r', '0000-00-00 00:00:00.000000'),
(190, 'Vietnam\r', '0000-00-00 00:00:00.000000'),
(191, 'Yemen\r', '0000-00-00 00:00:00.000000'),
(192, 'Yibuti\r', '0000-00-00 00:00:00.000000'),
(193, 'Zambia\r', '0000-00-00 00:00:00.000000'),
(194, 'Zimbabue\r', '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `country_department`
--

CREATE TABLE `country_department` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created` datetime(6) NOT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `country_department`
--

INSERT INTO `country_department` (`id`, `name`, `created`, `country_id`) VALUES
(1, 'Alta Verapaz', '0000-00-00 00:00:00.000000', 73),
(2, 'Baja Verapaz', '0000-00-00 00:00:00.000000', 73),
(3, 'Chimaltenango', '0000-00-00 00:00:00.000000', 73),
(4, 'Chiquimula', '0000-00-00 00:00:00.000000', 73),
(5, 'Petén', '0000-00-00 00:00:00.000000', 73),
(6, 'El Progreso', '0000-00-00 00:00:00.000000', 73),
(7, 'Quiché', '0000-00-00 00:00:00.000000', 73),
(8, 'Escuintla', '0000-00-00 00:00:00.000000', 73),
(9, 'Guatemala', '0000-00-00 00:00:00.000000', 73),
(10, 'Huehuetenango', '0000-00-00 00:00:00.000000', 73),
(11, 'Izabal', '0000-00-00 00:00:00.000000', 73),
(12, 'Jalapa', '0000-00-00 00:00:00.000000', 73),
(13, 'Jutiapa', '0000-00-00 00:00:00.000000', 73),
(14, 'Quetzaltenango', '0000-00-00 00:00:00.000000', 73),
(15, 'Retalhuleu', '0000-00-00 00:00:00.000000', 73),
(16, 'Sacatepéquez', '0000-00-00 00:00:00.000000', 73),
(17, 'San Marcos', '0000-00-00 00:00:00.000000', 73),
(18, 'Santa Rosa', '0000-00-00 00:00:00.000000', 73),
(19, 'Sololá', '0000-00-00 00:00:00.000000', 73),
(20, 'Suchitepéquez', '0000-00-00 00:00:00.000000', 73),
(21, 'Totonicapán', '0000-00-00 00:00:00.000000', 73),
(22, 'Zacapa', '0000-00-00 00:00:00.000000', 73);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `country_municipality`
--

CREATE TABLE `country_municipality` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created` datetime(6) NOT NULL,
  `department_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `country_municipality`
--

INSERT INTO `country_municipality` (`id`, `name`, `created`, `department_id`) VALUES
(1, 'Cobán', '0000-00-00 00:00:00.000000', 1),
(2, 'Santa Cruz Verapaz', '0000-00-00 00:00:00.000000', 1),
(3, 'San Cristobal Verapaz', '0000-00-00 00:00:00.000000', 1),
(4, 'Tactíc', '0000-00-00 00:00:00.000000', 1),
(5, 'Tamahú', '0000-00-00 00:00:00.000000', 1),
(6, 'San Miguel Tucurú', '0000-00-00 00:00:00.000000', 1),
(7, 'Panzos', '0000-00-00 00:00:00.000000', 1),
(8, 'Senahú', '0000-00-00 00:00:00.000000', 1),
(9, 'San Pedro Carchá', '0000-00-00 00:00:00.000000', 1),
(10, 'SanJuan Chamelco', '0000-00-00 00:00:00.000000', 1),
(11, 'Lanquín', '0000-00-00 00:00:00.000000', 1),
(12, 'Santa María Cahabón', '0000-00-00 00:00:00.000000', 1),
(13, 'Chisec', '0000-00-00 00:00:00.000000', 1),
(14, 'Chahal', '0000-00-00 00:00:00.000000', 1),
(15, 'Fray Bartolomé de las Casas', '0000-00-00 00:00:00.000000', 1),
(16, 'Santa Catarina La Tinta', '0000-00-00 00:00:00.000000', 1),
(17, 'Salamá', '0000-00-00 00:00:00.000000', 2),
(18, 'San Miguel Chicaj', '0000-00-00 00:00:00.000000', 2),
(19, 'Rabinal', '0000-00-00 00:00:00.000000', 2),
(20, 'Cubulco', '0000-00-00 00:00:00.000000', 2),
(21, 'Granados', '0000-00-00 00:00:00.000000', 2),
(22, 'Santa Cruz El Chol', '0000-00-00 00:00:00.000000', 2),
(23, 'San Jerónimo', '0000-00-00 00:00:00.000000', 2),
(24, 'Purulhá', '0000-00-00 00:00:00.000000', 2),
(25, 'Chimaltenango', '0000-00-00 00:00:00.000000', 3),
(26, 'San José Poaquil', '0000-00-00 00:00:00.000000', 3),
(27, 'San Martín Jilotepeque', '0000-00-00 00:00:00.000000', 3),
(28, 'San Juan Comalapa', '0000-00-00 00:00:00.000000', 3),
(29, 'Santa Apolonia', '0000-00-00 00:00:00.000000', 3),
(30, 'Tecpán Guatemala', '0000-00-00 00:00:00.000000', 3),
(31, 'Patzun', '0000-00-00 00:00:00.000000', 3),
(32, 'San Miguel Pochuta', '0000-00-00 00:00:00.000000', 3),
(33, 'Patzicia', '0000-00-00 00:00:00.000000', 3),
(34, 'Santa Cruz Balanyá', '0000-00-00 00:00:00.000000', 3),
(35, 'Acatenango', '0000-00-00 00:00:00.000000', 3),
(36, 'San Pedro Yepocapa', '0000-00-00 00:00:00.000000', 3),
(37, 'San Andrés Itzapa', '0000-00-00 00:00:00.000000', 3),
(38, 'Parramos', '0000-00-00 00:00:00.000000', 3),
(39, 'Zaragoza', '0000-00-00 00:00:00.000000', 3),
(40, 'El Tejar', '0000-00-00 00:00:00.000000', 3),
(41, 'Chiquimula', '0000-00-00 00:00:00.000000', 4),
(42, 'San José La Arada', '0000-00-00 00:00:00.000000', 4),
(43, 'San Juan Hermita', '0000-00-00 00:00:00.000000', 4),
(44, 'Jocotán', '0000-00-00 00:00:00.000000', 4),
(45, 'Camotán', '0000-00-00 00:00:00.000000', 4),
(46, 'Olopa', '0000-00-00 00:00:00.000000', 4),
(47, 'Esquipulas', '0000-00-00 00:00:00.000000', 4),
(48, 'Concepción Las Minas', '0000-00-00 00:00:00.000000', 4),
(49, 'Quezaltepeque', '0000-00-00 00:00:00.000000', 4),
(50, 'San Jacinto', '0000-00-00 00:00:00.000000', 4),
(51, 'Ipala', '0000-00-00 00:00:00.000000', 4),
(52, 'Flores', '0000-00-00 00:00:00.000000', 5),
(53, 'San José', '0000-00-00 00:00:00.000000', 5),
(54, 'San Benito', '0000-00-00 00:00:00.000000', 5),
(55, 'San Andrés', '0000-00-00 00:00:00.000000', 5),
(56, 'La Libertad', '0000-00-00 00:00:00.000000', 5),
(57, 'San Francisco', '0000-00-00 00:00:00.000000', 5),
(58, 'Santa Ana', '0000-00-00 00:00:00.000000', 5),
(59, 'Dolores', '0000-00-00 00:00:00.000000', 5),
(60, 'San Luis', '0000-00-00 00:00:00.000000', 5),
(61, 'Sayaxche', '0000-00-00 00:00:00.000000', 5),
(62, 'Melchor de Mencos', '0000-00-00 00:00:00.000000', 5),
(63, 'Poptún', '0000-00-00 00:00:00.000000', 5),
(64, 'Guastatoya', '0000-00-00 00:00:00.000000', 6),
(65, 'Morazán', '0000-00-00 00:00:00.000000', 6),
(66, 'San Agustín Acasaguastlan', '0000-00-00 00:00:00.000000', 6),
(67, 'San Cristóbal Acasaguastlan', '0000-00-00 00:00:00.000000', 6),
(68, 'El Jícaro', '0000-00-00 00:00:00.000000', 6),
(69, 'Sansare', '0000-00-00 00:00:00.000000', 6),
(70, 'Sanarate', '0000-00-00 00:00:00.000000', 6),
(71, 'San Antonio La Paz', '0000-00-00 00:00:00.000000', 6),
(72, 'Santa Cruz del Quiche', '0000-00-00 00:00:00.000000', 7),
(73, 'Chiche', '0000-00-00 00:00:00.000000', 7),
(74, 'Chinique', '0000-00-00 00:00:00.000000', 7),
(75, 'Zacualpa', '0000-00-00 00:00:00.000000', 7),
(76, 'Chajul', '0000-00-00 00:00:00.000000', 7),
(77, 'Santo Tomás Chichicstenango', '0000-00-00 00:00:00.000000', 7),
(78, 'Patzité', '0000-00-00 00:00:00.000000', 7),
(79, 'San Antonio Ilotenango', '0000-00-00 00:00:00.000000', 7),
(80, 'San Pedro Jocopilas', '0000-00-00 00:00:00.000000', 7),
(81, 'Cunén', '0000-00-00 00:00:00.000000', 7),
(82, 'San Juan Cotzal', '0000-00-00 00:00:00.000000', 7),
(83, 'Joyabaj', '0000-00-00 00:00:00.000000', 7),
(84, 'Santa María Nebaj', '0000-00-00 00:00:00.000000', 7),
(85, 'San Andrés Sajcabajá', '0000-00-00 00:00:00.000000', 7),
(86, 'San Miguel Uspatán', '0000-00-00 00:00:00.000000', 7),
(87, 'Sacapulas', '0000-00-00 00:00:00.000000', 7),
(88, 'San Bartolomé Jocotenango', '0000-00-00 00:00:00.000000', 7),
(89, 'Canilla', '0000-00-00 00:00:00.000000', 7),
(90, 'Chicaman', '0000-00-00 00:00:00.000000', 7),
(91, 'Playa Grnade - Ixcán', '0000-00-00 00:00:00.000000', 7),
(92, 'Pachalúm', '0000-00-00 00:00:00.000000', 7),
(93, 'Escuintla', '0000-00-00 00:00:00.000000', 8),
(94, 'Santa Lucía Cotzumalguapa', '0000-00-00 00:00:00.000000', 8),
(95, 'La Democracia', '0000-00-00 00:00:00.000000', 8),
(96, 'Siquinalá', '0000-00-00 00:00:00.000000', 8),
(97, 'Masagua', '0000-00-00 00:00:00.000000', 8),
(98, 'Pueblo Nuevo Tiquisate', '0000-00-00 00:00:00.000000', 8),
(99, 'La Gomera', '0000-00-00 00:00:00.000000', 8),
(100, 'Guanagazapa', '0000-00-00 00:00:00.000000', 8),
(101, 'Puerto de San José', '0000-00-00 00:00:00.000000', 8),
(102, 'Iztapa', '0000-00-00 00:00:00.000000', 8),
(103, 'Palín', '0000-00-00 00:00:00.000000', 8),
(104, 'San Vicente Pacaya', '0000-00-00 00:00:00.000000', 8),
(105, 'Nueva Concepción', '0000-00-00 00:00:00.000000', 8),
(106, 'Guatemala', '0000-00-00 00:00:00.000000', 9),
(107, 'Santa Catarina Pinula', '0000-00-00 00:00:00.000000', 9),
(108, 'San José Pinula', '0000-00-00 00:00:00.000000', 9),
(109, 'San José del Golfo', '0000-00-00 00:00:00.000000', 9),
(110, 'Palencia', '0000-00-00 00:00:00.000000', 9),
(111, 'Chinautla', '0000-00-00 00:00:00.000000', 9),
(112, 'San Pedro Ayampuc', '0000-00-00 00:00:00.000000', 9),
(113, 'Mixco', '0000-00-00 00:00:00.000000', 9),
(114, 'San Pedro Sacatepequez', '0000-00-00 00:00:00.000000', 9),
(115, 'San Juan Sacatepequez', '0000-00-00 00:00:00.000000', 9),
(116, 'San Raymundo', '0000-00-00 00:00:00.000000', 9),
(117, 'Chuarrancho', '0000-00-00 00:00:00.000000', 9),
(118, 'Fraijanes', '0000-00-00 00:00:00.000000', 9),
(119, 'Amatitlán', '0000-00-00 00:00:00.000000', 9),
(120, 'Villa Nueva', '0000-00-00 00:00:00.000000', 9),
(121, 'Villa Canales', '0000-00-00 00:00:00.000000', 9),
(122, 'San Miguel Petapa', '0000-00-00 00:00:00.000000', 9),
(123, 'Huehuetenango', '0000-00-00 00:00:00.000000', 10),
(124, 'Chiantla', '0000-00-00 00:00:00.000000', 10),
(125, 'Malacatancito', '0000-00-00 00:00:00.000000', 10),
(126, 'Cuilco', '0000-00-00 00:00:00.000000', 10),
(127, 'Nentón', '0000-00-00 00:00:00.000000', 10),
(128, 'San Pedro Necta', '0000-00-00 00:00:00.000000', 10),
(129, 'Jacaltenango', '0000-00-00 00:00:00.000000', 10),
(130, 'San Pedro Soloma', '0000-00-00 00:00:00.000000', 10),
(131, 'San Ildelfonso Ixtahuacán', '0000-00-00 00:00:00.000000', 10),
(132, 'Santa Bárbara', '0000-00-00 00:00:00.000000', 10),
(133, 'La Libertad', '0000-00-00 00:00:00.000000', 10),
(134, 'La Democracia', '0000-00-00 00:00:00.000000', 10),
(135, 'San Miguel Acatán', '0000-00-00 00:00:00.000000', 10),
(136, 'San Rafael La Independencia', '0000-00-00 00:00:00.000000', 10),
(137, 'Todos Santos Chuchcumatán', '0000-00-00 00:00:00.000000', 10),
(138, 'San Juan Atitán', '0000-00-00 00:00:00.000000', 10),
(139, 'Santa Eulalia', '0000-00-00 00:00:00.000000', 10),
(140, 'San Mateo Ixtatán', '0000-00-00 00:00:00.000000', 10),
(141, 'Colotenango', '0000-00-00 00:00:00.000000', 10),
(142, 'San Sebastián Huehuetenango', '0000-00-00 00:00:00.000000', 10),
(143, 'Tectitán', '0000-00-00 00:00:00.000000', 10),
(144, 'Concepción Huista', '0000-00-00 00:00:00.000000', 10),
(145, 'San Juan Ixcoy', '0000-00-00 00:00:00.000000', 10),
(146, 'San Antonio Huista', '0000-00-00 00:00:00.000000', 10),
(147, 'San Sebastián Coatán', '0000-00-00 00:00:00.000000', 10),
(148, 'Santa Cruz Barillas', '0000-00-00 00:00:00.000000', 10),
(149, 'Aguacatán', '0000-00-00 00:00:00.000000', 10),
(150, 'San Rafael Petzal', '0000-00-00 00:00:00.000000', 10),
(151, 'San Gaspar Ixchil', '0000-00-00 00:00:00.000000', 10),
(152, 'Santiago Chimaltenango', '0000-00-00 00:00:00.000000', 10),
(153, 'Santa Ana Huista', '0000-00-00 00:00:00.000000', 10),
(154, 'Puerto Barrios', '0000-00-00 00:00:00.000000', 11),
(155, 'Livingston', '0000-00-00 00:00:00.000000', 11),
(156, 'El Estor', '0000-00-00 00:00:00.000000', 11),
(157, 'Morales', '0000-00-00 00:00:00.000000', 11),
(158, 'Los Amates', '0000-00-00 00:00:00.000000', 11),
(159, 'Jalapa', '0000-00-00 00:00:00.000000', 12),
(160, 'San Pedro Pinula', '0000-00-00 00:00:00.000000', 12),
(161, 'San Luis Jilotepeque', '0000-00-00 00:00:00.000000', 12),
(162, 'San Manuel Chaparrón', '0000-00-00 00:00:00.000000', 12),
(163, 'San Carlos Alzatate', '0000-00-00 00:00:00.000000', 12),
(164, 'Monjas', '0000-00-00 00:00:00.000000', 12),
(165, 'Mataquescuintla', '0000-00-00 00:00:00.000000', 12),
(166, 'Jutiapa', '0000-00-00 00:00:00.000000', 13),
(167, 'El Progreso', '0000-00-00 00:00:00.000000', 13),
(168, 'Santa Catarina Mita', '0000-00-00 00:00:00.000000', 13),
(169, 'Agua Blanca', '0000-00-00 00:00:00.000000', 13),
(170, 'Asunción Mita', '0000-00-00 00:00:00.000000', 13),
(171, 'Yupiltepeque', '0000-00-00 00:00:00.000000', 13),
(172, 'Atescatempa', '0000-00-00 00:00:00.000000', 13),
(173, 'Jerez', '0000-00-00 00:00:00.000000', 13),
(174, 'El Adelanto', '0000-00-00 00:00:00.000000', 13),
(175, 'Zapotitlán', '0000-00-00 00:00:00.000000', 13),
(176, 'Comapa', '0000-00-00 00:00:00.000000', 13),
(177, 'Jalpatagua', '0000-00-00 00:00:00.000000', 13),
(178, 'Conguaco', '0000-00-00 00:00:00.000000', 13),
(179, 'Moyuta', '0000-00-00 00:00:00.000000', 13),
(180, 'Pasaco', '0000-00-00 00:00:00.000000', 13),
(181, 'San José Acatempa', '0000-00-00 00:00:00.000000', 13),
(182, 'Quezada', '0000-00-00 00:00:00.000000', 13),
(183, 'Quetzaltenango', '0000-00-00 00:00:00.000000', 14),
(184, 'Salcajá', '0000-00-00 00:00:00.000000', 14),
(185, 'Olintepeque', '0000-00-00 00:00:00.000000', 14),
(186, 'San Carlos Sija', '0000-00-00 00:00:00.000000', 14),
(187, 'Sibilia', '0000-00-00 00:00:00.000000', 14),
(188, 'Cabrican', '0000-00-00 00:00:00.000000', 14),
(189, 'Cajola', '0000-00-00 00:00:00.000000', 14),
(190, 'San Miguel Siguilça', '0000-00-00 00:00:00.000000', 14),
(191, 'San Juan Ostuncalco', '0000-00-00 00:00:00.000000', 14),
(192, 'San Mateo', '0000-00-00 00:00:00.000000', 14),
(193, 'Concepción Chiquirichapa', '0000-00-00 00:00:00.000000', 14),
(194, 'San Martín Sacatepequez', '0000-00-00 00:00:00.000000', 14),
(195, 'Almolonga', '0000-00-00 00:00:00.000000', 14),
(196, 'Cantel', '0000-00-00 00:00:00.000000', 14),
(197, 'Huitán', '0000-00-00 00:00:00.000000', 14),
(198, 'Zunil', '0000-00-00 00:00:00.000000', 14),
(199, 'Colomba', '0000-00-00 00:00:00.000000', 14),
(200, 'San Francisco La Unión', '0000-00-00 00:00:00.000000', 14),
(201, 'El Palmar', '0000-00-00 00:00:00.000000', 14),
(202, 'Coatepeque', '0000-00-00 00:00:00.000000', 14),
(203, 'Génova', '0000-00-00 00:00:00.000000', 14),
(204, 'Flores Costa Cuca', '0000-00-00 00:00:00.000000', 14),
(205, 'La Esperanza', '0000-00-00 00:00:00.000000', 14),
(206, 'Palestina de los Altos', '0000-00-00 00:00:00.000000', 14),
(207, 'Retalhuelu', '0000-00-00 00:00:00.000000', 15),
(208, 'San Sebastián', '0000-00-00 00:00:00.000000', 15),
(209, 'Santa Cruz Mulúa', '0000-00-00 00:00:00.000000', 15),
(210, 'San Martín Zapotitlán', '0000-00-00 00:00:00.000000', 15),
(211, 'San Felipe Retalhuleu', '0000-00-00 00:00:00.000000', 15),
(212, 'San Andrés Villa Seca', '0000-00-00 00:00:00.000000', 15),
(213, 'Champerico', '0000-00-00 00:00:00.000000', 15),
(214, 'Nuevo San Carlos', '0000-00-00 00:00:00.000000', 15),
(215, 'El Asintal', '0000-00-00 00:00:00.000000', 15),
(216, 'Antigua Guatemala', '0000-00-00 00:00:00.000000', 16),
(217, 'Jocotenango', '0000-00-00 00:00:00.000000', 16),
(218, 'Pastores', '0000-00-00 00:00:00.000000', 16),
(219, 'Sumpango', '0000-00-00 00:00:00.000000', 16),
(220, 'Santo Domingo Xenacoj', '0000-00-00 00:00:00.000000', 16),
(221, 'Santiago Sacatepequez', '0000-00-00 00:00:00.000000', 16),
(222, 'San Bartolomé Milpas Altas', '0000-00-00 00:00:00.000000', 16),
(223, 'San Lucas Sacatepequez', '0000-00-00 00:00:00.000000', 16),
(224, 'Santa Lucía Milpas Altas', '0000-00-00 00:00:00.000000', 16),
(225, 'Magdalena Milpas Altas', '0000-00-00 00:00:00.000000', 16),
(226, 'Santa María de Jesús', '0000-00-00 00:00:00.000000', 16),
(227, 'Ciudad Vieja', '0000-00-00 00:00:00.000000', 16),
(228, 'San Miguel Dueñas', '0000-00-00 00:00:00.000000', 16),
(229, 'San Juan Alotenango', '0000-00-00 00:00:00.000000', 16),
(230, 'San Antonio Aguas Calientes', '0000-00-00 00:00:00.000000', 16),
(231, 'Santa Catarina Barahona', '0000-00-00 00:00:00.000000', 16),
(232, 'San Marcos', '0000-00-00 00:00:00.000000', 17),
(233, 'San Pedro Sacatepéquez', '0000-00-00 00:00:00.000000', 17),
(234, 'Comitancillo', '0000-00-00 00:00:00.000000', 17),
(235, 'San Antonio Sacatepéquez', '0000-00-00 00:00:00.000000', 17),
(236, 'San Miguel Ixtahuacan', '0000-00-00 00:00:00.000000', 17),
(237, 'Concepción Tutuapa', '0000-00-00 00:00:00.000000', 17),
(238, 'Tacaná', '0000-00-00 00:00:00.000000', 17),
(239, 'Sibinal', '0000-00-00 00:00:00.000000', 17),
(240, 'Tajumulco', '0000-00-00 00:00:00.000000', 17),
(241, 'Tejutla', '0000-00-00 00:00:00.000000', 17),
(242, 'San Rafael Pié de la Cuesta', '0000-00-00 00:00:00.000000', 17),
(243, 'Nuevo Progreso', '0000-00-00 00:00:00.000000', 17),
(244, 'El Tumbador', '0000-00-00 00:00:00.000000', 17),
(245, 'San José El Rodeo', '0000-00-00 00:00:00.000000', 17),
(246, 'Malacatán', '0000-00-00 00:00:00.000000', 17),
(247, 'Catarina', '0000-00-00 00:00:00.000000', 17),
(248, 'Ayutla', '0000-00-00 00:00:00.000000', 17),
(249, 'Ocos', '0000-00-00 00:00:00.000000', 17),
(250, 'San Pablo', '0000-00-00 00:00:00.000000', 17),
(251, 'El Quetzal', '0000-00-00 00:00:00.000000', 17),
(252, 'La Reforma', '0000-00-00 00:00:00.000000', 17),
(253, 'Pajapita', '0000-00-00 00:00:00.000000', 17),
(254, 'Ixchiguan', '0000-00-00 00:00:00.000000', 17),
(255, 'San José Ojetenán', '0000-00-00 00:00:00.000000', 17),
(256, 'San Cristóbal Cucho', '0000-00-00 00:00:00.000000', 17),
(257, 'Sipacapa', '0000-00-00 00:00:00.000000', 17),
(258, 'Esquipulas Palo Gordo', '0000-00-00 00:00:00.000000', 17),
(259, 'Río Blanco', '0000-00-00 00:00:00.000000', 17),
(260, 'San Lorenzo', '0000-00-00 00:00:00.000000', 17),
(261, 'Cuilapa', '0000-00-00 00:00:00.000000', 18),
(262, 'Berberena', '0000-00-00 00:00:00.000000', 18),
(263, 'San Rosa de Lima', '0000-00-00 00:00:00.000000', 18),
(264, 'Casillas', '0000-00-00 00:00:00.000000', 18),
(265, 'San Rafael Las Flores', '0000-00-00 00:00:00.000000', 18),
(266, 'Oratorio', '0000-00-00 00:00:00.000000', 18),
(267, 'San Juan Tecuaco', '0000-00-00 00:00:00.000000', 18),
(268, 'Chiquimulilla', '0000-00-00 00:00:00.000000', 18),
(269, 'Taxisco', '0000-00-00 00:00:00.000000', 18),
(270, 'Santa María Ixhuatan', '0000-00-00 00:00:00.000000', 18),
(271, 'Guazacapán', '0000-00-00 00:00:00.000000', 18),
(272, 'Santa Cruz Naranjo', '0000-00-00 00:00:00.000000', 18),
(273, 'Pueblo Nuevo Viñas', '0000-00-00 00:00:00.000000', 18),
(274, 'Nueva Santa Rosa', '0000-00-00 00:00:00.000000', 18),
(275, 'Sololá', '0000-00-00 00:00:00.000000', 19),
(276, 'San José Chacaya', '0000-00-00 00:00:00.000000', 19),
(277, 'Santa María Visitación', '0000-00-00 00:00:00.000000', 19),
(278, 'Santa Lucía Utatlán', '0000-00-00 00:00:00.000000', 19),
(279, 'Nahualá', '0000-00-00 00:00:00.000000', 19),
(280, 'Santa Catarina Ixtahuacán', '0000-00-00 00:00:00.000000', 19),
(281, 'Santa Clara La Laguna', '0000-00-00 00:00:00.000000', 19),
(282, 'Concepción', '0000-00-00 00:00:00.000000', 19),
(283, 'San Andrés Semetabaj', '0000-00-00 00:00:00.000000', 19),
(284, 'Panajachel', '0000-00-00 00:00:00.000000', 19),
(285, 'Santa Catarina Palopó', '0000-00-00 00:00:00.000000', 19),
(286, 'San Antonio Palopó', '0000-00-00 00:00:00.000000', 19),
(287, 'San Lucas Tolimán', '0000-00-00 00:00:00.000000', 19),
(288, 'Santa Cruz La Laguna', '0000-00-00 00:00:00.000000', 19),
(289, 'Sna Pablo La Laguna', '0000-00-00 00:00:00.000000', 19),
(290, 'San Marcos La Laguna', '0000-00-00 00:00:00.000000', 19),
(291, 'San Juan La Laguna', '0000-00-00 00:00:00.000000', 19),
(292, 'San Pedro La Laguna', '0000-00-00 00:00:00.000000', 19),
(293, 'Santiago Atitlán', '0000-00-00 00:00:00.000000', 19),
(294, 'Mazatenango', '0000-00-00 00:00:00.000000', 20),
(295, 'Cuyotenango', '0000-00-00 00:00:00.000000', 20),
(296, 'San Francisco Zapotitlán', '0000-00-00 00:00:00.000000', 20),
(297, 'San Bernardino', '0000-00-00 00:00:00.000000', 20),
(298, 'San José El Ídolo', '0000-00-00 00:00:00.000000', 20),
(299, 'Santo Domingo Suchitepequez', '0000-00-00 00:00:00.000000', 20),
(300, 'San Lorenzo', '0000-00-00 00:00:00.000000', 20),
(301, 'Samayac', '0000-00-00 00:00:00.000000', 20),
(302, 'San Pablo Jocopilas', '0000-00-00 00:00:00.000000', 20),
(303, 'San Antonio Suchitepéquez', '0000-00-00 00:00:00.000000', 20),
(304, 'San Miguel Panán', '0000-00-00 00:00:00.000000', 20),
(305, 'San Gabriel', '0000-00-00 00:00:00.000000', 20),
(306, 'Chicacao', '0000-00-00 00:00:00.000000', 20),
(307, 'Patulul', '0000-00-00 00:00:00.000000', 20),
(308, 'Santa Bárbara', '0000-00-00 00:00:00.000000', 20),
(309, 'San Juan Bautista', '0000-00-00 00:00:00.000000', 20),
(310, 'Santo Tomás La Unión', '0000-00-00 00:00:00.000000', 20),
(311, 'Zunilito', '0000-00-00 00:00:00.000000', 20),
(312, 'Pueblo Nuevo Suchitepéquez', '0000-00-00 00:00:00.000000', 20),
(313, 'Río Bravo', '0000-00-00 00:00:00.000000', 20),
(314, 'Totonicapán', '0000-00-00 00:00:00.000000', 21),
(315, 'San Cristóbal Totonicapán', '0000-00-00 00:00:00.000000', 21),
(316, 'San Francisco El Alto', '0000-00-00 00:00:00.000000', 21),
(317, 'San Andrés Xecul', '0000-00-00 00:00:00.000000', 21),
(318, 'Momostenango', '0000-00-00 00:00:00.000000', 21),
(319, 'Santa María Chiquimula', '0000-00-00 00:00:00.000000', 21),
(320, 'Santa Lucía La Reforma', '0000-00-00 00:00:00.000000', 21),
(321, 'San Bartolo Aguas Calientes', '0000-00-00 00:00:00.000000', 21),
(322, 'Zacapa', '0000-00-00 00:00:00.000000', 22),
(323, 'Estanzuela', '0000-00-00 00:00:00.000000', 22),
(324, 'Río Hondo', '0000-00-00 00:00:00.000000', 22),
(325, 'Gualán', '0000-00-00 00:00:00.000000', 22),
(326, 'Teculután', '0000-00-00 00:00:00.000000', 22),
(327, 'Usumatlán', '0000-00-00 00:00:00.000000', 22),
(328, 'Cabañas', '0000-00-00 00:00:00.000000', 22),
(329, 'San Diego', '0000-00-00 00:00:00.000000', 22),
(330, 'La Unión', '0000-00-00 00:00:00.000000', 22),
(331, 'Huite', '0000-00-00 00:00:00.000000', 22);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2018-06-30 21:37:26.490985', '1', 'Guatemala', 1, '[{\"added\": {}}]', 9, 1),
(2, '2018-06-30 21:37:47.684147', '1', 'El Progreso', 1, '[{\"added\": {}}]', 7, 1),
(3, '2018-06-30 21:38:09.286914', '1', 'Guastatoya', 1, '[{\"added\": {}}]', 8, 1),
(4, '2018-06-30 21:51:57.523938', '1', 'Guatemala', 3, '', 9, 1),
(5, '2018-06-30 23:47:28.084489', '194', '', 3, '', 9, 1),
(6, '2018-06-30 23:47:28.182578', '193', '', 3, '', 9, 1),
(7, '2018-06-30 23:47:28.238574', '192', '', 3, '', 9, 1),
(8, '2018-06-30 23:47:28.337566', '191', '', 3, '', 9, 1),
(9, '2018-06-30 23:47:28.369575', '190', '', 3, '', 9, 1),
(10, '2018-06-30 23:47:28.404573', '189', '', 3, '', 9, 1),
(11, '2018-06-30 23:47:28.436572', '188', '', 3, '', 9, 1),
(12, '2018-06-30 23:47:28.482440', '187', '', 3, '', 9, 1),
(13, '2018-06-30 23:47:28.515437', '186', '', 3, '', 9, 1),
(14, '2018-06-30 23:47:28.549448', '185', '', 3, '', 9, 1),
(15, '2018-06-30 23:47:28.580447', '184', '', 3, '', 9, 1),
(16, '2018-06-30 23:47:28.626443', '183', '', 3, '', 9, 1),
(17, '2018-06-30 23:47:28.658441', '182', '', 3, '', 9, 1),
(18, '2018-06-30 23:47:28.693439', '181', '', 3, '', 9, 1),
(19, '2018-06-30 23:47:28.725437', '180', '', 3, '', 9, 1),
(20, '2018-06-30 23:47:28.782432', '179', '', 3, '', 9, 1),
(21, '2018-06-30 23:47:28.802430', '178', '', 3, '', 9, 1),
(22, '2018-06-30 23:47:28.848428', '177', '', 3, '', 9, 1),
(23, '2018-06-30 23:47:28.880427', '176', '', 3, '', 9, 1),
(24, '2018-06-30 23:47:28.915425', '175', '', 3, '', 9, 1),
(25, '2018-06-30 23:47:28.947421', '174', '', 3, '', 9, 1),
(26, '2018-06-30 23:47:28.982420', '173', '', 3, '', 9, 1),
(27, '2018-06-30 23:47:29.013418', '172', '', 3, '', 9, 1),
(28, '2018-06-30 23:47:29.048416', '171', '', 3, '', 9, 1),
(29, '2018-06-30 23:47:29.080413', '170', '', 3, '', 9, 1),
(30, '2018-06-30 23:47:29.114410', '169', '', 3, '', 9, 1),
(31, '2018-06-30 23:47:29.147409', '168', '', 3, '', 9, 1),
(32, '2018-06-30 23:47:29.182407', '167', '', 3, '', 9, 1),
(33, '2018-06-30 23:47:29.215404', '166', '', 3, '', 9, 1),
(34, '2018-06-30 23:47:29.249406', '165', '', 3, '', 9, 1),
(35, '2018-06-30 23:47:29.280403', '164', '', 3, '', 9, 1),
(36, '2018-06-30 23:47:29.316398', '163', '', 3, '', 9, 1),
(37, '2018-06-30 23:47:29.357396', '162', '', 3, '', 9, 1),
(38, '2018-06-30 23:47:29.392393', '161', '', 3, '', 9, 1),
(39, '2018-06-30 23:47:29.435390', '160', '', 3, '', 9, 1),
(40, '2018-06-30 23:47:29.476387', '159', '', 3, '', 9, 1),
(41, '2018-06-30 23:47:29.513386', '158', '', 3, '', 9, 1),
(42, '2018-06-30 23:47:29.547383', '157', '', 3, '', 9, 1),
(43, '2018-06-30 23:47:29.603405', '156', '', 3, '', 9, 1),
(44, '2018-06-30 23:47:29.634426', '155', '', 3, '', 9, 1),
(45, '2018-06-30 23:47:29.681433', '154', '', 3, '', 9, 1),
(46, '2018-06-30 23:47:29.712429', '153', '', 3, '', 9, 1),
(47, '2018-06-30 23:47:29.747438', '152', '', 3, '', 9, 1),
(48, '2018-06-30 23:47:29.789430', '151', '', 3, '', 9, 1),
(49, '2018-06-30 23:47:29.825432', '150', '', 3, '', 9, 1),
(50, '2018-06-30 23:47:29.856445', '149', '', 3, '', 9, 1),
(51, '2018-06-30 23:47:29.891449', '148', '', 3, '', 9, 1),
(52, '2018-06-30 23:47:29.923459', '147', '', 3, '', 9, 1),
(53, '2018-06-30 23:47:29.958459', '146', '', 3, '', 9, 1),
(54, '2018-06-30 23:47:30.002453', '145', '', 3, '', 9, 1),
(55, '2018-06-30 23:47:30.034049', '144', '', 3, '', 9, 1),
(56, '2018-06-30 23:47:30.069051', '143', '', 3, '', 9, 1),
(57, '2018-06-30 23:47:30.125049', '142', '', 3, '', 9, 1),
(58, '2018-06-30 23:47:30.200072', '141', '', 3, '', 9, 1),
(59, '2018-06-30 23:47:30.257070', '140', '', 3, '', 9, 1),
(60, '2018-06-30 23:47:30.302066', '139', '', 3, '', 9, 1),
(61, '2018-06-30 23:47:30.385072', '138', '', 3, '', 9, 1),
(62, '2018-06-30 23:47:30.472112', '137', '', 3, '', 9, 1),
(63, '2018-06-30 23:47:30.513143', '136', '', 3, '', 9, 1),
(64, '2018-06-30 23:47:30.544121', '135', '', 3, '', 9, 1),
(65, '2018-06-30 23:47:30.590124', '134', '', 3, '', 9, 1),
(66, '2018-06-30 23:47:30.633122', '133', '', 3, '', 9, 1),
(67, '2018-06-30 23:47:30.668120', '132', '', 3, '', 9, 1),
(68, '2018-06-30 23:47:30.735116', '131', '', 3, '', 9, 1),
(69, '2018-06-30 23:47:30.782112', '130', '', 3, '', 9, 1),
(70, '2018-06-30 23:47:30.835110', '129', '', 3, '', 9, 1),
(71, '2018-06-30 23:47:30.870107', '128', '', 3, '', 9, 1),
(72, '2018-06-30 23:47:30.912104', '127', '', 3, '', 9, 1),
(73, '2018-06-30 23:47:30.960101', '126', '', 3, '', 9, 1),
(74, '2018-06-30 23:47:31.001097', '125', '', 3, '', 9, 1),
(75, '2018-06-30 23:47:31.044095', '124', '', 3, '', 9, 1),
(76, '2018-06-30 23:47:31.090093', '123', '', 3, '', 9, 1),
(77, '2018-06-30 23:47:31.139090', '122', '', 3, '', 9, 1),
(78, '2018-06-30 23:47:31.179089', '121', '', 3, '', 9, 1),
(79, '2018-06-30 23:47:31.244083', '120', '', 3, '', 9, 1),
(80, '2018-06-30 23:47:31.393073', '119', '', 3, '', 9, 1),
(81, '2018-06-30 23:47:31.467068', '118', '', 3, '', 9, 1),
(82, '2018-06-30 23:47:31.522063', '117', '', 3, '', 9, 1),
(83, '2018-06-30 23:47:31.566062', '116', '', 3, '', 9, 1),
(84, '2018-06-30 23:47:31.701162', '115', '', 3, '', 9, 1),
(85, '2018-06-30 23:47:31.756530', '114', '', 3, '', 9, 1),
(86, '2018-06-30 23:47:31.847541', '113', '', 3, '', 9, 1),
(87, '2018-06-30 23:47:31.912539', '112', '', 3, '', 9, 1),
(88, '2018-06-30 23:47:31.966533', '111', '', 3, '', 9, 1),
(89, '2018-06-30 23:47:32.033982', '110', '', 3, '', 9, 1),
(90, '2018-06-30 23:47:32.088990', '109', '', 3, '', 9, 1),
(91, '2018-06-30 23:47:32.144037', '108', '', 3, '', 9, 1),
(92, '2018-06-30 23:47:32.199051', '107', '', 3, '', 9, 1),
(93, '2018-06-30 23:47:32.244075', '106', '', 3, '', 9, 1),
(94, '2018-06-30 23:47:32.276252', '105', '', 3, '', 9, 1),
(95, '2018-06-30 23:47:32.310259', '104', '', 3, '', 9, 1),
(96, '2018-06-30 23:47:32.342268', '103', '', 3, '', 9, 1),
(97, '2018-06-30 23:47:32.376655', '102', '', 3, '', 9, 1),
(98, '2018-06-30 23:47:32.408665', '101', '', 3, '', 9, 1),
(99, '2018-06-30 23:47:32.443957', '100', '', 3, '', 9, 1),
(100, '2018-06-30 23:47:32.486670', '99', '', 3, '', 9, 1),
(101, '2018-06-30 23:47:32.521967', '98', '', 3, '', 9, 1),
(102, '2018-06-30 23:47:32.554975', '97', '', 3, '', 9, 1),
(103, '2018-06-30 23:47:32.619982', '96', '', 3, '', 9, 1),
(104, '2018-06-30 23:47:32.834967', '95', '', 3, '', 9, 1),
(105, '2018-06-30 23:48:40.864152', '94', '', 3, '', 9, 1),
(106, '2018-06-30 23:48:40.972147', '93', '', 3, '', 9, 1),
(107, '2018-06-30 23:48:41.013142', '92', '', 3, '', 9, 1),
(108, '2018-06-30 23:48:41.050140', '91', '', 3, '', 9, 1),
(109, '2018-06-30 23:48:41.091137', '90', '', 3, '', 9, 1),
(110, '2018-06-30 23:48:41.149134', '89', '', 3, '', 9, 1),
(111, '2018-06-30 23:48:41.192130', '88', '', 3, '', 9, 1),
(112, '2018-06-30 23:48:41.273127', '87', '', 3, '', 9, 1),
(113, '2018-06-30 23:48:41.377120', '86', '', 3, '', 9, 1),
(114, '2018-06-30 23:48:41.526108', '85', '', 3, '', 9, 1),
(115, '2018-06-30 23:48:41.621104', '84', '', 3, '', 9, 1),
(116, '2018-06-30 23:48:41.660100', '83', '', 3, '', 9, 1),
(117, '2018-06-30 23:48:41.690100', '82', '', 3, '', 9, 1),
(118, '2018-06-30 23:48:41.726097', '81', '', 3, '', 9, 1),
(119, '2018-06-30 23:48:41.926084', '80', '', 3, '', 9, 1),
(120, '2018-06-30 23:48:41.955081', '79', '', 3, '', 9, 1),
(121, '2018-06-30 23:48:41.992078', '78', '', 3, '', 9, 1),
(122, '2018-06-30 23:48:42.028076', '77', '', 3, '', 9, 1),
(123, '2018-06-30 23:48:42.170068', '76', '', 3, '', 9, 1),
(124, '2018-06-30 23:48:42.234061', '75', '', 3, '', 9, 1),
(125, '2018-06-30 23:48:42.292073', '74', '', 3, '', 9, 1),
(126, '2018-06-30 23:48:42.392104', '73', '', 3, '', 9, 1),
(127, '2018-06-30 23:48:42.445122', '72', '', 3, '', 9, 1),
(128, '2018-06-30 23:48:42.498144', '71', '', 3, '', 9, 1),
(129, '2018-06-30 23:48:42.558160', '70', '', 3, '', 9, 1),
(130, '2018-06-30 23:48:42.635160', '69', '', 3, '', 9, 1),
(131, '2018-06-30 23:48:42.698165', '68', '', 3, '', 9, 1),
(132, '2018-06-30 23:48:42.784187', '67', '', 3, '', 9, 1),
(133, '2018-06-30 23:48:42.862215', '66', '', 3, '', 9, 1),
(134, '2018-06-30 23:48:42.926225', '65', '', 3, '', 9, 1),
(135, '2018-06-30 23:48:42.997217', '64', '', 3, '', 9, 1),
(136, '2018-06-30 23:48:43.057214', '63', '', 3, '', 9, 1),
(137, '2018-06-30 23:48:43.087211', '62', '', 3, '', 9, 1),
(138, '2018-06-30 23:48:43.135209', '61', '', 3, '', 9, 1),
(139, '2018-06-30 23:48:43.165210', '60', '', 3, '', 9, 1),
(140, '2018-06-30 23:48:43.212202', '59', '', 3, '', 9, 1),
(141, '2018-06-30 23:48:43.243202', '58', '', 3, '', 9, 1),
(142, '2018-06-30 23:48:43.279199', '57', '', 3, '', 9, 1),
(143, '2018-06-30 23:48:43.309197', '56', '', 3, '', 9, 1),
(144, '2018-06-30 23:48:43.347197', '55', '', 3, '', 9, 1),
(145, '2018-06-30 23:48:43.375192', '54', '', 3, '', 9, 1),
(146, '2018-06-30 23:48:43.412189', '53', '', 3, '', 9, 1),
(147, '2018-06-30 23:48:43.442188', '52', '', 3, '', 9, 1),
(148, '2018-06-30 23:48:43.480189', '51', '', 3, '', 9, 1),
(149, '2018-06-30 23:48:43.509186', '50', '', 3, '', 9, 1),
(150, '2018-06-30 23:48:43.545181', '49', '', 3, '', 9, 1),
(151, '2018-06-30 23:48:43.575180', '48', '', 3, '', 9, 1),
(152, '2018-06-30 23:48:43.612177', '47', '', 3, '', 9, 1),
(153, '2018-06-30 23:48:43.642175', '46', '', 3, '', 9, 1),
(154, '2018-06-30 23:48:43.723171', '45', '', 3, '', 9, 1),
(155, '2018-06-30 23:48:43.753168', '44', '', 3, '', 9, 1),
(156, '2018-06-30 23:48:43.790166', '43', '', 3, '', 9, 1),
(157, '2018-06-30 23:48:43.820165', '42', '', 3, '', 9, 1),
(158, '2018-06-30 23:48:43.856162', '41', '', 3, '', 9, 1),
(159, '2018-06-30 23:48:43.886159', '40', '', 3, '', 9, 1),
(160, '2018-06-30 23:48:43.923157', '39', '', 3, '', 9, 1),
(161, '2018-06-30 23:48:43.957155', '38', '', 3, '', 9, 1),
(162, '2018-06-30 23:48:44.031384', '37', '', 3, '', 9, 1),
(163, '2018-06-30 23:48:44.111428', '36', '', 3, '', 9, 1),
(164, '2018-06-30 23:48:44.164550', '35', '', 3, '', 9, 1),
(165, '2018-06-30 23:48:44.211525', '34', '', 3, '', 9, 1),
(166, '2018-06-30 23:48:44.241524', '33', '', 3, '', 9, 1),
(167, '2018-06-30 23:48:44.278538', '32', '', 3, '', 9, 1),
(168, '2018-06-30 23:48:44.354542', '31', '', 3, '', 9, 1),
(169, '2018-06-30 23:48:44.400538', '30', '', 3, '', 9, 1),
(170, '2018-06-30 23:48:44.430545', '29', '', 3, '', 9, 1),
(171, '2018-06-30 23:48:44.467198', '28', '', 3, '', 9, 1),
(172, '2018-06-30 23:48:44.496208', '27', '', 3, '', 9, 1),
(173, '2018-06-30 23:48:44.534206', '26', '', 3, '', 9, 1),
(174, '2018-06-30 23:48:44.563204', '25', '', 3, '', 9, 1),
(175, '2018-06-30 23:48:44.600218', '24', '', 3, '', 9, 1),
(176, '2018-06-30 23:48:44.629222', '23', '', 3, '', 9, 1),
(177, '2018-06-30 23:48:44.667249', '22', '', 3, '', 9, 1),
(178, '2018-06-30 23:48:44.696264', '21', '', 3, '', 9, 1),
(179, '2018-06-30 23:48:44.733254', '20', '', 3, '', 9, 1),
(180, '2018-06-30 23:48:44.763153', '19', '', 3, '', 9, 1),
(181, '2018-06-30 23:48:44.800151', '18', '', 3, '', 9, 1),
(182, '2018-06-30 23:48:44.829160', '17', '', 3, '', 9, 1),
(183, '2018-06-30 23:48:44.877159', '16', '', 3, '', 9, 1),
(184, '2018-06-30 23:48:44.906541', '15', '', 3, '', 9, 1),
(185, '2018-06-30 23:48:44.955554', '14', '', 3, '', 9, 1),
(186, '2018-06-30 23:48:44.984553', '13', '', 3, '', 9, 1),
(187, '2018-06-30 23:48:45.021551', '12', '', 3, '', 9, 1),
(188, '2018-06-30 23:48:45.053549', '11', '', 3, '', 9, 1),
(189, '2018-06-30 23:48:45.088546', '10', '', 3, '', 9, 1),
(190, '2018-06-30 23:48:45.118544', '9', '', 3, '', 9, 1),
(191, '2018-06-30 23:48:45.154542', '8', '', 3, '', 9, 1),
(192, '2018-06-30 23:48:45.184540', '7', '', 3, '', 9, 1),
(193, '2018-06-30 23:48:45.221539', '6', '', 3, '', 9, 1),
(194, '2018-06-30 23:48:45.251537', '5', '', 3, '', 9, 1),
(195, '2018-06-30 23:48:45.287533', '4', '', 3, '', 9, 1),
(196, '2018-06-30 23:48:45.317532', '3', '', 3, '', 9, 1),
(197, '2018-06-30 23:48:45.354529', '2', '', 3, '', 9, 1),
(198, '2018-06-30 23:48:45.386526', '1', '', 3, '', 9, 1),
(199, '2018-06-30 23:53:28.509977', '194', '', 3, '', 9, 1),
(200, '2018-06-30 23:53:28.651961', '193', '', 3, '', 9, 1),
(201, '2018-06-30 23:53:28.681959', '192', '', 3, '', 9, 1),
(202, '2018-06-30 23:53:28.718956', '191', '', 3, '', 9, 1),
(203, '2018-06-30 23:53:28.748955', '190', '', 3, '', 9, 1),
(204, '2018-06-30 23:53:28.784951', '189', '', 3, '', 9, 1),
(205, '2018-06-30 23:53:28.814950', '188', '', 3, '', 9, 1),
(206, '2018-06-30 23:53:28.851948', '187', '', 3, '', 9, 1),
(207, '2018-06-30 23:53:28.881947', '186', '', 3, '', 9, 1),
(208, '2018-06-30 23:53:28.918944', '185', '', 3, '', 9, 1),
(209, '2018-06-30 23:53:28.947941', '184', '', 3, '', 9, 1),
(210, '2018-06-30 23:53:28.974939', '183', '', 3, '', 9, 1),
(211, '2018-06-30 23:53:29.003938', '182', '', 3, '', 9, 1),
(212, '2018-06-30 23:53:29.051936', '181', '', 3, '', 9, 1),
(213, '2018-06-30 23:53:29.080933', '180', '', 3, '', 9, 1),
(214, '2018-06-30 23:53:29.117930', '179', '', 3, '', 9, 1),
(215, '2018-06-30 23:53:29.158928', '178', '', 3, '', 9, 1),
(216, '2018-06-30 23:53:29.195926', '177', '', 3, '', 9, 1),
(217, '2018-06-30 23:53:29.225924', '176', '', 3, '', 9, 1),
(218, '2018-06-30 23:53:29.261920', '175', '', 3, '', 9, 1),
(219, '2018-06-30 23:53:29.317918', '174', '', 3, '', 9, 1),
(220, '2018-06-30 23:53:29.347916', '173', '', 3, '', 9, 1),
(221, '2018-06-30 23:53:29.383911', '172', '', 3, '', 9, 1),
(222, '2018-06-30 23:53:29.413910', '171', '', 3, '', 9, 1),
(223, '2018-06-30 23:53:29.462917', '170', '', 3, '', 9, 1),
(224, '2018-06-30 23:53:29.586908', '169', '', 3, '', 9, 1),
(225, '2018-06-30 23:53:29.624906', '168', '', 3, '', 9, 1),
(226, '2018-06-30 23:53:29.661904', '167', '', 3, '', 9, 1),
(227, '2018-06-30 23:53:29.691902', '166', '', 3, '', 9, 1),
(228, '2018-06-30 23:53:29.728012', '165', '', 3, '', 9, 1),
(229, '2018-06-30 23:53:29.758012', '164', '', 3, '', 9, 1),
(230, '2018-06-30 23:53:29.795009', '163', '', 3, '', 9, 1),
(231, '2018-06-30 23:53:29.825007', '162', '', 3, '', 9, 1),
(232, '2018-06-30 23:53:29.920024', '161', '', 3, '', 9, 1),
(233, '2018-06-30 23:53:29.973030', '160', '', 3, '', 9, 1),
(234, '2018-06-30 23:53:30.002026', '159', '', 3, '', 9, 1),
(235, '2018-06-30 23:53:30.039488', '158', '', 3, '', 9, 1),
(236, '2018-06-30 23:53:30.068492', '157', '', 3, '', 9, 1),
(237, '2018-06-30 23:53:30.116500', '156', '', 3, '', 9, 1),
(238, '2018-06-30 23:53:30.146517', '155', '', 3, '', 9, 1),
(239, '2018-06-30 23:53:30.183525', '154', '', 3, '', 9, 1),
(240, '2018-06-30 23:53:30.213419', '153', '', 3, '', 9, 1),
(241, '2018-06-30 23:53:30.249414', '152', '', 3, '', 9, 1),
(242, '2018-06-30 23:53:30.280091', '151', '', 3, '', 9, 1),
(243, '2018-06-30 23:53:30.316132', '150', '', 3, '', 9, 1),
(244, '2018-06-30 23:53:30.346146', '149', '', 3, '', 9, 1),
(245, '2018-06-30 23:53:30.383147', '148', '', 3, '', 9, 1),
(246, '2018-06-30 23:53:30.413144', '147', '', 3, '', 9, 1),
(247, '2018-06-30 23:53:30.449149', '146', '', 3, '', 9, 1),
(248, '2018-06-30 23:53:30.479145', '145', '', 3, '', 9, 1),
(249, '2018-06-30 23:53:30.516144', '144', '', 3, '', 9, 1),
(250, '2018-06-30 23:53:30.546142', '143', '', 3, '', 9, 1),
(251, '2018-06-30 23:53:30.583140', '142', '', 3, '', 9, 1),
(252, '2018-06-30 23:53:30.612138', '141', '', 3, '', 9, 1),
(253, '2018-06-30 23:53:30.653138', '140', '', 3, '', 9, 1),
(254, '2018-06-30 23:53:30.727130', '139', '', 3, '', 9, 1),
(255, '2018-06-30 23:53:30.757128', '138', '', 3, '', 9, 1),
(256, '2018-06-30 23:53:30.852122', '137', '', 3, '', 9, 1),
(257, '2018-06-30 23:53:30.918118', '136', '', 3, '', 9, 1),
(258, '2018-06-30 23:53:31.126104', '135', '', 3, '', 9, 1),
(259, '2018-06-30 23:53:31.171101', '134', '', 3, '', 9, 1),
(260, '2018-06-30 23:53:31.200098', '133', '', 3, '', 9, 1),
(261, '2018-06-30 23:53:31.248095', '132', '', 3, '', 9, 1),
(262, '2018-06-30 23:53:31.296092', '131', '', 3, '', 9, 1),
(263, '2018-06-30 23:53:31.344090', '130', '', 3, '', 9, 1),
(264, '2018-06-30 23:53:31.416086', '129', '', 3, '', 9, 1),
(265, '2018-06-30 23:53:31.470080', '128', '', 3, '', 9, 1),
(266, '2018-06-30 23:53:31.703067', '127', '', 3, '', 9, 1),
(267, '2018-06-30 23:53:31.796060', '126', '', 3, '', 9, 1),
(268, '2018-06-30 23:53:31.860057', '125', '', 3, '', 9, 1),
(269, '2018-06-30 23:53:31.921051', '124', '', 3, '', 9, 1),
(270, '2018-06-30 23:53:31.984048', '123', '', 3, '', 9, 1),
(271, '2018-06-30 23:53:32.036043', '122', '', 3, '', 9, 1),
(272, '2018-06-30 23:53:32.066041', '121', '', 3, '', 9, 1),
(273, '2018-06-30 23:53:32.103058', '120', '', 3, '', 9, 1),
(274, '2018-06-30 23:53:32.133067', '119', '', 3, '', 9, 1),
(275, '2018-06-30 23:53:32.169063', '118', '', 3, '', 9, 1),
(276, '2018-06-30 23:53:32.199061', '117', '', 3, '', 9, 1),
(277, '2018-06-30 23:53:32.236061', '116', '', 3, '', 9, 1),
(278, '2018-06-30 23:53:32.265346', '115', '', 3, '', 9, 1),
(279, '2018-06-30 23:53:32.302385', '114', '', 3, '', 9, 1),
(280, '2018-06-30 23:53:32.331960', '113', '', 3, '', 9, 1),
(281, '2018-06-30 23:53:32.369006', '112', '', 3, '', 9, 1),
(282, '2018-06-30 23:53:32.399006', '111', '', 3, '', 9, 1),
(283, '2018-06-30 23:53:32.447002', '110', '', 3, '', 9, 1),
(284, '2018-06-30 23:53:32.477030', '109', '', 3, '', 9, 1),
(285, '2018-06-30 23:53:32.524163', '108', '', 3, '', 9, 1),
(286, '2018-06-30 23:53:32.656015', '107', '', 3, '', 9, 1),
(287, '2018-06-30 23:53:32.702010', '106', '', 3, '', 9, 1),
(288, '2018-06-30 23:53:32.732010', '105', '', 3, '', 9, 1),
(289, '2018-06-30 23:53:32.768886', '104', '', 3, '', 9, 1),
(290, '2018-06-30 23:53:32.798894', '103', '', 3, '', 9, 1),
(291, '2018-06-30 23:53:32.834891', '102', '', 3, '', 9, 1),
(292, '2018-06-30 23:53:32.864889', '101', '', 3, '', 9, 1),
(293, '2018-06-30 23:53:32.901901', '100', '', 3, '', 9, 1),
(294, '2018-06-30 23:53:32.931898', '99', '', 3, '', 9, 1),
(295, '2018-06-30 23:53:32.967896', '98', '', 3, '', 9, 1),
(296, '2018-06-30 23:53:32.997902', '97', '', 3, '', 9, 1),
(297, '2018-06-30 23:53:33.034904', '96', '', 3, '', 9, 1),
(298, '2018-06-30 23:53:33.064900', '95', '', 3, '', 9, 1),
(299, '2018-06-30 23:54:10.461896', '94', '', 3, '', 9, 1),
(300, '2018-06-30 23:54:10.510893', '93', '', 3, '', 9, 1),
(301, '2018-06-30 23:54:10.548891', '92', '', 3, '', 9, 1),
(302, '2018-06-30 23:54:10.577888', '91', '', 3, '', 9, 1),
(303, '2018-06-30 23:54:10.615885', '90', '', 3, '', 9, 1),
(304, '2018-06-30 23:54:10.644884', '89', '', 3, '', 9, 1),
(305, '2018-06-30 23:54:10.681881', '88', '', 3, '', 9, 1),
(306, '2018-06-30 23:54:10.711883', '87', '', 3, '', 9, 1),
(307, '2018-06-30 23:54:10.748877', '86', '', 3, '', 9, 1),
(308, '2018-06-30 23:54:10.790875', '85', '', 3, '', 9, 1),
(309, '2018-06-30 23:54:10.836869', '84', '', 3, '', 9, 1),
(310, '2018-06-30 23:54:10.868870', '83', '', 3, '', 9, 1),
(311, '2018-06-30 23:54:10.959867', '82', '', 3, '', 9, 1),
(312, '2018-06-30 23:54:10.999860', '81', '', 3, '', 9, 1),
(313, '2018-06-30 23:54:11.038858', '80', '', 3, '', 9, 1),
(314, '2018-06-30 23:54:11.078855', '79', '', 3, '', 9, 1),
(315, '2018-06-30 23:54:11.114854', '78', '', 3, '', 9, 1),
(316, '2018-06-30 23:54:11.143851', '77', '', 3, '', 9, 1),
(317, '2018-06-30 23:54:11.180848', '76', '', 3, '', 9, 1),
(318, '2018-06-30 23:54:11.210848', '75', '', 3, '', 9, 1),
(319, '2018-06-30 23:54:11.247844', '74', '', 3, '', 9, 1),
(320, '2018-06-30 23:54:11.321838', '73', '', 3, '', 9, 1),
(321, '2018-06-30 23:54:11.388834', '72', '', 3, '', 9, 1),
(322, '2018-06-30 23:54:11.513827', '71', '', 3, '', 9, 1),
(323, '2018-06-30 23:54:11.715814', '70', '', 3, '', 9, 1),
(324, '2018-06-30 23:54:11.746811', '69', '', 3, '', 9, 1),
(325, '2018-06-30 23:54:11.780809', '68', '', 3, '', 9, 1),
(326, '2018-06-30 23:54:11.816817', '67', '', 3, '', 9, 1),
(327, '2018-06-30 23:54:11.847817', '66', '', 3, '', 9, 1),
(328, '2018-06-30 23:54:11.902813', '65', '', 3, '', 9, 1),
(329, '2018-06-30 23:54:11.945821', '64', '', 3, '', 9, 1),
(330, '2018-06-30 23:54:11.991817', '63', '', 3, '', 9, 1),
(331, '2018-06-30 23:54:12.128222', '62', '', 3, '', 9, 1),
(332, '2018-06-30 23:54:12.169234', '61', '', 3, '', 9, 1),
(333, '2018-06-30 23:54:12.186230', '60', '', 3, '', 9, 1),
(334, '2018-06-30 23:54:12.246120', '59', '', 3, '', 9, 1),
(335, '2018-06-30 23:54:12.348113', '58', '', 3, '', 9, 1),
(336, '2018-06-30 23:54:12.428110', '57', '', 3, '', 9, 1),
(337, '2018-06-30 23:54:12.549100', '56', '', 3, '', 9, 1),
(338, '2018-06-30 23:54:12.639095', '55', '', 3, '', 9, 1),
(339, '2018-06-30 23:54:12.709090', '54', '', 3, '', 9, 1),
(340, '2018-06-30 23:54:12.779085', '53', '', 3, '', 9, 1),
(341, '2018-06-30 23:54:12.820083', '52', '', 3, '', 9, 1),
(342, '2018-06-30 23:54:12.873080', '51', '', 3, '', 9, 1),
(343, '2018-06-30 23:54:12.908077', '50', '', 3, '', 9, 1),
(344, '2018-06-30 23:54:12.935077', '49', '', 3, '', 9, 1),
(345, '2018-06-30 23:54:12.964075', '48', '', 3, '', 9, 1),
(346, '2018-06-30 23:54:13.012069', '47', '', 3, '', 9, 1),
(347, '2018-06-30 23:54:13.041069', '46', '', 3, '', 9, 1),
(348, '2018-06-30 23:54:13.079066', '45', '', 3, '', 9, 1),
(349, '2018-06-30 23:54:13.111068', '44', '', 3, '', 9, 1),
(350, '2018-06-30 23:54:13.157063', '43', '', 3, '', 9, 1),
(351, '2018-06-30 23:54:13.197059', '42', '', 3, '', 9, 1),
(352, '2018-06-30 23:54:13.234055', '41', '', 3, '', 9, 1),
(353, '2018-06-30 23:54:13.331050', '40', '', 3, '', 9, 1),
(354, '2018-06-30 23:54:13.363048', '39', '', 3, '', 9, 1),
(355, '2018-06-30 23:54:13.402045', '38', '', 3, '', 9, 1),
(356, '2018-06-30 23:54:13.429045', '37', '', 3, '', 9, 1),
(357, '2018-06-30 23:54:13.467041', '36', '', 3, '', 9, 1),
(358, '2018-06-30 23:54:13.496038', '35', '', 3, '', 9, 1),
(359, '2018-06-30 23:54:13.534036', '34', '', 3, '', 9, 1),
(360, '2018-06-30 23:54:13.616030', '33', '', 3, '', 9, 1),
(361, '2018-06-30 23:54:13.678031', '32', '', 3, '', 9, 1),
(362, '2018-06-30 23:54:13.900012', '31', '', 3, '', 9, 1),
(363, '2018-06-30 23:54:13.929010', '30', '', 3, '', 9, 1),
(364, '2018-06-30 23:54:13.956010', '29', '', 3, '', 9, 1),
(365, '2018-06-30 23:54:14.037004', '28', '', 3, '', 9, 1),
(366, '2018-06-30 23:54:14.073002', '27', '', 3, '', 9, 1),
(367, '2018-06-30 23:54:14.121998', '26', '', 3, '', 9, 1),
(368, '2018-06-30 23:54:14.161994', '25', '', 3, '', 9, 1),
(369, '2018-06-30 23:54:14.237990', '24', '', 3, '', 9, 1),
(370, '2018-06-30 23:54:14.272989', '23', '', 3, '', 9, 1),
(371, '2018-06-30 23:54:14.310988', '22', '', 3, '', 9, 1),
(372, '2018-06-30 23:54:14.338983', '21', '', 3, '', 9, 1),
(373, '2018-06-30 23:54:14.376981', '20', '', 3, '', 9, 1),
(374, '2018-06-30 23:54:14.405979', '19', '', 3, '', 9, 1),
(375, '2018-06-30 23:54:14.443978', '18', '', 3, '', 9, 1),
(376, '2018-06-30 23:54:14.473975', '17', '', 3, '', 9, 1),
(377, '2018-06-30 23:54:14.510973', '16', '', 3, '', 9, 1),
(378, '2018-06-30 23:54:14.538970', '15', '', 3, '', 9, 1),
(379, '2018-06-30 23:54:14.582967', '14', '', 3, '', 9, 1),
(380, '2018-06-30 23:54:14.698960', '13', '', 3, '', 9, 1),
(381, '2018-06-30 23:54:14.900947', '12', '', 3, '', 9, 1),
(382, '2018-06-30 23:54:15.019940', '11', '', 3, '', 9, 1),
(383, '2018-06-30 23:54:15.081935', '10', '', 3, '', 9, 1),
(384, '2018-06-30 23:54:15.120932', '9', '', 3, '', 9, 1),
(385, '2018-06-30 23:54:15.176929', '8', '', 3, '', 9, 1),
(386, '2018-06-30 23:54:15.219925', '7', '', 3, '', 9, 1),
(387, '2018-06-30 23:54:15.260922', '6', '', 3, '', 9, 1),
(388, '2018-06-30 23:54:15.346920', '5', '', 3, '', 9, 1),
(389, '2018-06-30 23:54:15.398916', '4', '', 3, '', 9, 1),
(390, '2018-06-30 23:54:15.495922', '3', '', 3, '', 9, 1),
(391, '2018-06-30 23:54:15.576933', '2', '', 3, '', 9, 1),
(392, '2018-06-30 23:54:15.615929', '1', '', 3, '', 9, 1),
(393, '2018-07-01 00:59:28.314458', '331', 'Huite', 3, '', 7, 1),
(394, '2018-07-01 00:59:28.386453', '330', 'La Unión', 3, '', 7, 1),
(395, '2018-07-01 00:59:28.420451', '329', 'San Diego', 3, '', 7, 1),
(396, '2018-07-01 00:59:28.448449', '328', 'Cabañas', 3, '', 7, 1),
(397, '2018-07-01 00:59:28.475447', '327', 'Usumatlán', 3, '', 7, 1),
(398, '2018-07-01 00:59:28.509445', '326', 'Teculután', 3, '', 7, 1),
(399, '2018-07-01 00:59:28.542443', '325', 'Gualán', 3, '', 7, 1),
(400, '2018-07-01 00:59:28.582442', '324', 'Río Hondo', 3, '', 7, 1),
(401, '2018-07-01 00:59:28.630437', '323', 'Estanzuela', 3, '', 7, 1),
(402, '2018-07-01 00:59:28.675436', '322', 'Zacapa', 3, '', 7, 1),
(403, '2018-07-01 00:59:28.709432', '321', 'San Bartolo Aguas Calientes', 3, '', 7, 1),
(404, '2018-07-01 00:59:28.758428', '320', 'Santa Lucía La Reforma', 3, '', 7, 1),
(405, '2018-07-01 00:59:28.792427', '319', 'Santa María Chiquimula', 3, '', 7, 1),
(406, '2018-07-01 00:59:28.859421', '318', 'Momostenango', 3, '', 7, 1),
(407, '2018-07-01 00:59:28.905419', '317', 'San Andrés Xecul', 3, '', 7, 1),
(408, '2018-07-01 00:59:28.965416', '316', 'San Francisco El Alto', 3, '', 7, 1),
(409, '2018-07-01 00:59:29.033411', '315', 'San Cristóbal Totonicapán', 3, '', 7, 1),
(410, '2018-07-01 00:59:29.101407', '314', 'Totonicapán', 3, '', 7, 1),
(411, '2018-07-01 00:59:29.142405', '313', 'Río Bravo', 3, '', 7, 1),
(412, '2018-07-01 00:59:29.201400', '312', 'Pueblo Nuevo Suchitepéquez', 3, '', 7, 1),
(413, '2018-07-01 00:59:29.265398', '311', 'Zunilito', 3, '', 7, 1),
(414, '2018-07-01 00:59:29.322393', '310', 'Santo Tomás La Unión', 3, '', 7, 1),
(415, '2018-07-01 00:59:29.373391', '309', 'San Juan Bautista', 3, '', 7, 1),
(416, '2018-07-01 00:59:29.440385', '308', 'Santa Bárbara', 3, '', 7, 1),
(417, '2018-07-01 00:59:29.469384', '307', 'Patulul', 3, '', 7, 1),
(418, '2018-07-01 00:59:29.521379', '306', 'Chicacao', 3, '', 7, 1),
(419, '2018-07-01 00:59:29.567900', '305', 'San Gabriel', 3, '', 7, 1),
(420, '2018-07-01 00:59:29.631894', '304', 'San Miguel Panán', 3, '', 7, 1),
(421, '2018-07-01 00:59:29.679890', '303', 'San Antonio Suchitepéquez', 3, '', 7, 1),
(422, '2018-07-01 00:59:29.729887', '302', 'San Pablo Jocopilas', 3, '', 7, 1),
(423, '2018-07-01 00:59:29.787883', '301', 'Samayac', 3, '', 7, 1),
(424, '2018-07-01 00:59:29.900877', '300', 'San Lorenzo', 3, '', 7, 1),
(425, '2018-07-01 00:59:29.934873', '299', 'Santo Domingo Suchitepequez', 3, '', 7, 1),
(426, '2018-07-01 00:59:29.986390', '298', 'San José El Ídolo', 3, '', 7, 1),
(427, '2018-07-01 00:59:30.038436', '297', 'San Bernardino', 3, '', 7, 1),
(428, '2018-07-01 00:59:30.106431', '296', 'San Francisco Zapotitlán', 3, '', 7, 1),
(429, '2018-07-01 00:59:30.161478', '295', 'Cuyotenango', 3, '', 7, 1),
(430, '2018-07-01 00:59:30.217996', '294', 'Mazatenango', 3, '', 7, 1),
(431, '2018-07-01 00:59:30.273990', '293', 'Santiago Atitlán', 3, '', 7, 1),
(432, '2018-07-01 00:59:30.327988', '292', 'San Pedro La Laguna', 3, '', 7, 1),
(433, '2018-07-01 00:59:30.361984', '291', 'San Juan La Laguna', 3, '', 7, 1),
(434, '2018-07-01 00:59:30.416981', '290', 'San Marcos La Laguna', 3, '', 7, 1),
(435, '2018-07-01 00:59:30.450979', '289', 'Sna Pablo La Laguna', 3, '', 7, 1),
(436, '2018-07-01 00:59:30.498975', '288', 'Santa Cruz La Laguna', 3, '', 7, 1),
(437, '2018-07-01 00:59:30.541973', '287', 'San Lucas Tolimán', 3, '', 7, 1),
(438, '2018-07-01 00:59:30.584971', '286', 'San Antonio Palopó', 3, '', 7, 1),
(439, '2018-07-01 00:59:30.633967', '285', 'Santa Catarina Palopó', 3, '', 7, 1),
(440, '2018-07-01 00:59:30.747963', '284', 'Panajachel', 3, '', 7, 1),
(441, '2018-07-01 00:59:30.800956', '283', 'San Andrés Semetabaj', 3, '', 7, 1),
(442, '2018-07-01 00:59:30.852953', '282', 'Concepción', 3, '', 7, 1),
(443, '2018-07-01 00:59:30.922948', '281', 'Santa Clara La Laguna', 3, '', 7, 1),
(444, '2018-07-01 00:59:30.999942', '280', 'Santa Catarina Ixtahuacán', 3, '', 7, 1),
(445, '2018-07-01 00:59:31.099936', '279', 'Nahualá', 3, '', 7, 1),
(446, '2018-07-01 00:59:31.136935', '278', 'Santa Lucía Utatlán', 3, '', 7, 1),
(447, '2018-07-01 00:59:31.171930', '277', 'Santa María Visitación', 3, '', 7, 1),
(448, '2018-07-01 00:59:31.212929', '276', 'San José Chacaya', 3, '', 7, 1),
(449, '2018-07-01 00:59:31.277925', '275', 'Sololá', 3, '', 7, 1),
(450, '2018-07-01 00:59:31.327922', '274', 'Nueva Santa Rosa', 3, '', 7, 1),
(451, '2018-07-01 00:59:31.355921', '273', 'Pueblo Nuevo Viñas', 3, '', 7, 1),
(452, '2018-07-01 00:59:31.382918', '272', 'Santa Cruz Naranjo', 3, '', 7, 1),
(453, '2018-07-01 00:59:31.411916', '271', 'Guazacapán', 3, '', 7, 1),
(454, '2018-07-01 00:59:31.449913', '270', 'Santa María Ixhuatan', 3, '', 7, 1),
(455, '2018-07-01 00:59:31.476911', '269', 'Taxisco', 3, '', 7, 1),
(456, '2018-07-01 00:59:31.508910', '268', 'Chiquimulilla', 3, '', 7, 1),
(457, '2018-07-01 00:59:31.542907', '267', 'San Juan Tecuaco', 3, '', 7, 1),
(458, '2018-07-01 00:59:31.599907', '266', 'Oratorio', 3, '', 7, 1),
(459, '2018-07-01 00:59:31.649901', '265', 'San Rafael Las Flores', 3, '', 7, 1),
(460, '2018-07-01 00:59:31.687898', '264', 'Casillas', 3, '', 7, 1),
(461, '2018-07-01 00:59:31.726896', '263', 'San Rosa de Lima', 3, '', 7, 1),
(462, '2018-07-01 00:59:31.756894', '262', 'Berberena', 3, '', 7, 1),
(463, '2018-07-01 00:59:31.821890', '261', 'Cuilapa', 3, '', 7, 1),
(464, '2018-07-01 00:59:31.849889', '260', 'San Lorenzo', 3, '', 7, 1),
(465, '2018-07-01 00:59:31.876885', '259', 'Río Blanco', 3, '', 7, 1),
(466, '2018-07-01 00:59:31.904883', '258', 'Esquipulas Palo Gordo', 3, '', 7, 1),
(467, '2018-07-01 00:59:31.932884', '257', 'Sipacapa', 3, '', 7, 1),
(468, '2018-07-01 00:59:31.995401', '256', 'San Cristóbal Cucho', 3, '', 7, 1),
(469, '2018-07-01 00:59:32.037454', '255', 'San José Ojetenán', 3, '', 7, 1),
(470, '2018-07-01 00:59:32.076449', '254', 'Ixchiguan', 3, '', 7, 1),
(471, '2018-07-01 00:59:32.125445', '253', 'Pajapita', 3, '', 7, 1),
(472, '2018-07-01 00:59:32.198964', '252', 'La Reforma', 3, '', 7, 1),
(473, '2018-07-01 00:59:32.242961', '251', 'El Quetzal', 3, '', 7, 1),
(474, '2018-07-01 00:59:32.287957', '250', 'San Pablo', 3, '', 7, 1),
(475, '2018-07-01 00:59:32.349954', '249', 'Ocos', 3, '', 7, 1),
(476, '2018-07-01 00:59:32.404953', '248', 'Ayutla', 3, '', 7, 1),
(477, '2018-07-01 00:59:32.467945', '247', 'Catarina', 3, '', 7, 1),
(478, '2018-07-01 00:59:32.532941', '246', 'Malacatán', 3, '', 7, 1),
(479, '2018-07-01 00:59:32.608462', '245', 'San José El Rodeo', 3, '', 7, 1),
(480, '2018-07-01 00:59:32.642459', '244', 'El Tumbador', 3, '', 7, 1),
(481, '2018-07-01 00:59:32.683457', '243', 'Nuevo Progreso', 3, '', 7, 1),
(482, '2018-07-01 00:59:32.720455', '242', 'San Rafael Pié de la Cuesta', 3, '', 7, 1),
(483, '2018-07-01 00:59:32.763451', '241', 'Tejutla', 3, '', 7, 1),
(484, '2018-07-01 00:59:32.808965', '240', 'Tajumulco', 3, '', 7, 1),
(485, '2018-07-01 00:59:32.875960', '239', 'Sibinal', 3, '', 7, 1),
(486, '2018-07-01 00:59:32.944959', '238', 'Tacaná', 3, '', 7, 1),
(487, '2018-07-01 00:59:33.071989', '237', 'Concepción Tutuapa', 3, '', 7, 1),
(488, '2018-07-01 00:59:33.191982', '236', 'San Miguel Ixtahuacan', 3, '', 7, 1),
(489, '2018-07-01 00:59:33.218979', '235', 'San Antonio Sacatepéquez', 3, '', 7, 1),
(490, '2018-07-01 00:59:33.257978', '234', 'Comitancillo', 3, '', 7, 1),
(491, '2018-07-01 00:59:33.285976', '233', 'San Pedro Sacatepéquez', 3, '', 7, 1),
(492, '2018-07-01 00:59:33.315973', '232', 'San Marcos', 3, '', 7, 1),
(493, '2018-07-01 00:59:49.004698', '231', 'Santa Catarina Barahona', 3, '', 7, 1),
(494, '2018-07-01 00:59:49.065695', '230', 'San Antonio Aguas Calientes', 3, '', 7, 1),
(495, '2018-07-01 00:59:49.114690', '229', 'San Juan Alotenango', 3, '', 7, 1),
(496, '2018-07-01 00:59:49.153688', '228', 'San Miguel Dueñas', 3, '', 7, 1),
(497, '2018-07-01 00:59:49.199685', '227', 'Ciudad Vieja', 3, '', 7, 1),
(498, '2018-07-01 00:59:49.236682', '226', 'Santa María de Jesús', 3, '', 7, 1),
(499, '2018-07-01 00:59:49.264680', '225', 'Magdalena Milpas Altas', 3, '', 7, 1),
(500, '2018-07-01 00:59:49.302677', '224', 'Santa Lucía Milpas Altas', 3, '', 7, 1),
(501, '2018-07-01 00:59:49.354674', '223', 'San Lucas Sacatepequez', 3, '', 7, 1),
(502, '2018-07-01 00:59:49.402671', '222', 'San Bartolomé Milpas Altas', 3, '', 7, 1),
(503, '2018-07-01 00:59:49.430669', '221', 'Santiago Sacatepequez', 3, '', 7, 1),
(504, '2018-07-01 00:59:49.468668', '220', 'Santo Domingo Xenacoj', 3, '', 7, 1),
(505, '2018-07-01 00:59:49.552661', '219', 'Sumpango', 3, '', 7, 1),
(506, '2018-07-01 00:59:49.590658', '218', 'Pastores', 3, '', 7, 1),
(507, '2018-07-01 00:59:49.623656', '217', 'Jocotenango', 3, '', 7, 1),
(508, '2018-07-01 00:59:49.696695', '216', 'Antigua Guatemala', 3, '', 7, 1),
(509, '2018-07-01 00:59:49.748694', '215', 'El Asintal', 3, '', 7, 1),
(510, '2018-07-01 00:59:49.790689', '214', 'Nuevo San Carlos', 3, '', 7, 1),
(511, '2018-07-01 00:59:49.863684', '213', 'Champerico', 3, '', 7, 1),
(512, '2018-07-01 00:59:49.970677', '212', 'San Andrés Villa Seca', 3, '', 7, 1),
(513, '2018-07-01 00:59:50.038673', '211', 'San Felipe Retalhuleu', 3, '', 7, 1),
(514, '2018-07-01 00:59:50.092669', '210', 'San Martín Zapotitlán', 3, '', 7, 1),
(515, '2018-07-01 00:59:50.140667', '209', 'Santa Cruz Mulúa', 3, '', 7, 1),
(516, '2018-07-01 00:59:50.191662', '208', 'San Sebastián', 3, '', 7, 1),
(517, '2018-07-01 00:59:50.233660', '207', 'Retalhuelu', 3, '', 7, 1),
(518, '2018-07-01 00:59:50.273657', '206', 'Palestina de los Altos', 3, '', 7, 1),
(519, '2018-07-01 00:59:50.311656', '205', 'La Esperanza', 3, '', 7, 1),
(520, '2018-07-01 00:59:50.352652', '204', 'Flores Costa Cuca', 3, '', 7, 1),
(521, '2018-07-01 00:59:50.389651', '203', 'Génova', 3, '', 7, 1),
(522, '2018-07-01 00:59:50.428648', '202', 'Coatepeque', 3, '', 7, 1),
(523, '2018-07-01 00:59:50.467645', '201', 'El Palmar', 3, '', 7, 1),
(524, '2018-07-01 00:59:50.508645', '200', 'San Francisco La Unión', 3, '', 7, 1),
(525, '2018-07-01 00:59:50.556642', '199', 'Colomba', 3, '', 7, 1),
(526, '2018-07-01 00:59:50.657635', '198', 'Zunil', 3, '', 7, 1),
(527, '2018-07-01 00:59:50.811622', '197', 'Huitán', 3, '', 7, 1),
(528, '2018-07-01 00:59:50.861618', '196', 'Cantel', 3, '', 7, 1),
(529, '2018-07-01 00:59:50.900617', '195', 'Almolonga', 3, '', 7, 1),
(530, '2018-07-01 00:59:50.928615', '194', 'San Martín Sacatepequez', 3, '', 7, 1),
(531, '2018-07-01 00:59:50.966613', '193', 'Concepción Chiquirichapa', 3, '', 7, 1),
(532, '2018-07-01 00:59:50.995614', '192', 'San Mateo', 3, '', 7, 1),
(533, '2018-07-01 00:59:51.048608', '191', 'San Juan Ostuncalco', 3, '', 7, 1),
(534, '2018-07-01 00:59:51.101604', '190', 'San Miguel Siguilça', 3, '', 7, 1),
(535, '2018-07-01 00:59:51.175599', '189', 'Cajola', 3, '', 7, 1),
(536, '2018-07-01 00:59:51.211597', '188', 'Cabrican', 3, '', 7, 1),
(537, '2018-07-01 00:59:51.250595', '187', 'Sibilia', 3, '', 7, 1),
(538, '2018-07-01 00:59:51.307592', '186', 'San Carlos Sija', 3, '', 7, 1),
(539, '2018-07-01 00:59:51.338590', '185', 'Olintepeque', 3, '', 7, 1),
(540, '2018-07-01 00:59:51.382586', '184', 'Salcajá', 3, '', 7, 1),
(541, '2018-07-01 00:59:51.416582', '183', 'Quetzaltenango', 3, '', 7, 1),
(542, '2018-07-01 00:59:51.456582', '182', 'Quezada', 3, '', 7, 1),
(543, '2018-07-01 00:59:51.539576', '181', 'San José Acatempa', 3, '', 7, 1),
(544, '2018-07-01 00:59:51.588572', '180', 'Pasaco', 3, '', 7, 1),
(545, '2018-07-01 00:59:51.616570', '179', 'Moyuta', 3, '', 7, 1),
(546, '2018-07-01 00:59:51.655570', '178', 'Conguaco', 3, '', 7, 1),
(547, '2018-07-01 00:59:51.682566', '177', 'Jalpatagua', 3, '', 7, 1),
(548, '2018-07-01 00:59:51.699565', '176', 'Comapa', 3, '', 7, 1),
(549, '2018-07-01 00:59:51.739666', '175', 'Zapotitlán', 3, '', 7, 1),
(550, '2018-07-01 00:59:51.788079', '174', 'El Adelanto', 3, '', 7, 1),
(551, '2018-07-01 00:59:51.817079', '173', 'Jerez', 3, '', 7, 1),
(552, '2018-07-01 00:59:51.855075', '172', 'Atescatempa', 3, '', 7, 1),
(553, '2018-07-01 00:59:51.883074', '171', 'Yupiltepeque', 3, '', 7, 1),
(554, '2018-07-01 00:59:51.928590', '170', 'Asunción Mita', 3, '', 7, 1),
(555, '2018-07-01 00:59:51.961109', '169', 'Agua Blanca', 3, '', 7, 1),
(556, '2018-07-01 00:59:52.011105', '168', 'Santa Catarina Mita', 3, '', 7, 1),
(557, '2018-07-01 00:59:52.054101', '167', 'El Progreso', 3, '', 7, 1),
(558, '2018-07-01 00:59:52.083100', '166', 'Jutiapa', 3, '', 7, 1),
(559, '2018-07-01 00:59:52.120099', '165', 'Mataquescuintla', 3, '', 7, 1),
(560, '2018-07-01 00:59:52.150098', '164', 'Monjas', 3, '', 7, 1),
(561, '2018-07-01 00:59:52.187093', '163', 'San Carlos Alzatate', 3, '', 7, 1),
(562, '2018-07-01 00:59:52.217093', '162', 'San Manuel Chaparrón', 3, '', 7, 1),
(563, '2018-07-01 00:59:52.267088', '161', 'San Luis Jilotepeque', 3, '', 7, 1),
(564, '2018-07-01 00:59:52.318084', '160', 'San Pedro Pinula', 3, '', 7, 1),
(565, '2018-07-01 00:59:52.377081', '159', 'Jalapa', 3, '', 7, 1),
(566, '2018-07-01 00:59:52.440077', '158', 'Los Amates', 3, '', 7, 1),
(567, '2018-07-01 00:59:52.477074', '157', 'Morales', 3, '', 7, 1),
(568, '2018-07-01 00:59:52.516072', '156', 'El Estor', 3, '', 7, 1),
(569, '2018-07-01 00:59:52.556070', '155', 'Livingston', 3, '', 7, 1),
(570, '2018-07-01 00:59:52.593066', '154', 'Puerto Barrios', 3, '', 7, 1),
(571, '2018-07-01 00:59:52.645064', '153', 'Santa Ana Huista', 3, '', 7, 1),
(572, '2018-07-01 00:59:52.687060', '152', 'Santiago Chimaltenango', 3, '', 7, 1),
(573, '2018-07-01 00:59:52.732059', '151', 'San Gaspar Ixchil', 3, '', 7, 1),
(574, '2018-07-01 00:59:52.766056', '150', 'San Rafael Petzal', 3, '', 7, 1),
(575, '2018-07-01 00:59:52.804054', '149', 'Aguacatán', 3, '', 7, 1),
(576, '2018-07-01 00:59:52.843050', '148', 'Santa Cruz Barillas', 3, '', 7, 1),
(577, '2018-07-01 00:59:52.886047', '147', 'San Sebastián Coatán', 3, '', 7, 1),
(578, '2018-07-01 00:59:52.915046', '146', 'San Antonio Huista', 3, '', 7, 1),
(579, '2018-07-01 00:59:52.980042', '145', 'San Juan Ixcoy', 3, '', 7, 1),
(580, '2018-07-01 00:59:53.016041', '144', 'Concepción Huista', 3, '', 7, 1),
(581, '2018-07-01 00:59:53.056037', '143', 'Tectitán', 3, '', 7, 1),
(582, '2018-07-01 00:59:53.092034', '142', 'San Sebastián Huehuetenango', 3, '', 7, 1),
(583, '2018-07-01 00:59:53.153030', '141', 'Colotenango', 3, '', 7, 1),
(584, '2018-07-01 00:59:53.197089', '140', 'San Mateo Ixtatán', 3, '', 7, 1),
(585, '2018-07-01 00:59:53.263087', '139', 'Santa Eulalia', 3, '', 7, 1),
(586, '2018-07-01 00:59:53.297085', '138', 'San Juan Atitán', 3, '', 7, 1),
(587, '2018-07-01 00:59:53.359081', '137', 'Todos Santos Chuchcumatán', 3, '', 7, 1),
(588, '2018-07-01 00:59:53.374078', '136', 'San Rafael La Independencia', 3, '', 7, 1),
(589, '2018-07-01 00:59:53.414076', '135', 'San Miguel Acatán', 3, '', 7, 1),
(590, '2018-07-01 00:59:53.463593', '134', 'La Democracia', 3, '', 7, 1),
(591, '2018-07-01 00:59:53.507590', '133', 'La Libertad', 3, '', 7, 1),
(592, '2018-07-01 00:59:53.536590', '132', 'Santa Bárbara', 3, '', 7, 1),
(593, '2018-07-01 01:00:15.067745', '131', 'San Ildelfonso Ixtahuacán', 3, '', 7, 1),
(594, '2018-07-01 01:00:15.127255', '130', 'San Pedro Soloma', 3, '', 7, 1),
(595, '2018-07-01 01:00:15.200250', '129', 'Jacaltenango', 3, '', 7, 1),
(596, '2018-07-01 01:00:15.239248', '128', 'San Pedro Necta', 3, '', 7, 1),
(597, '2018-07-01 01:00:15.278245', '127', 'Nentón', 3, '', 7, 1),
(598, '2018-07-01 01:00:15.321242', '126', 'Cuilco', 3, '', 7, 1),
(599, '2018-07-01 01:00:15.350241', '125', 'Malacatancito', 3, '', 7, 1),
(600, '2018-07-01 01:00:15.388238', '124', 'Chiantla', 3, '', 7, 1),
(601, '2018-07-01 01:00:15.416236', '123', 'Huehuetenango', 3, '', 7, 1),
(602, '2018-07-01 01:00:15.454235', '122', 'San Miguel Petapa', 3, '', 7, 1),
(603, '2018-07-01 01:00:15.483232', '121', 'Villa Canales', 3, '', 7, 1),
(604, '2018-07-01 01:00:15.533229', '120', 'Villa Nueva', 3, '', 7, 1),
(605, '2018-07-01 01:00:15.578629', '119', 'Amatitlán', 3, '', 7, 1),
(606, '2018-07-01 01:00:15.621386', '118', 'Fraijanes', 3, '', 7, 1),
(607, '2018-07-01 01:00:15.650385', '117', 'Chuarrancho', 3, '', 7, 1),
(608, '2018-07-01 01:00:15.687381', '116', 'San Raymundo', 3, '', 7, 1),
(609, '2018-07-01 01:00:15.719381', '115', 'San Juan Sacatepequez', 3, '', 7, 1),
(610, '2018-07-01 01:00:15.767377', '114', 'San Pedro Sacatepequez', 3, '', 7, 1),
(611, '2018-07-01 01:00:15.805376', '113', 'Mixco', 3, '', 7, 1),
(612, '2018-07-01 01:00:15.931366', '112', 'San Pedro Ayampuc', 3, '', 7, 1),
(613, '2018-07-01 01:00:16.051357', '111', 'Chinautla', 3, '', 7, 1),
(614, '2018-07-01 01:00:16.114355', '110', 'Palencia', 3, '', 7, 1),
(615, '2018-07-01 01:00:16.153351', '109', 'San José del Golfo', 3, '', 7, 1),
(616, '2018-07-01 01:00:16.171350', '108', 'San José Pinula', 3, '', 7, 1),
(617, '2018-07-01 01:00:16.209348', '107', 'Santa Catarina Pinula', 3, '', 7, 1),
(618, '2018-07-01 01:00:16.237345', '106', 'Guatemala', 3, '', 7, 1),
(619, '2018-07-01 01:00:16.275345', '105', 'Nueva Concepción', 3, '', 7, 1),
(620, '2018-07-01 01:00:16.304342', '104', 'San Vicente Pacaya', 3, '', 7, 1),
(621, '2018-07-01 01:00:16.342339', '103', 'Palín', 3, '', 7, 1),
(622, '2018-07-01 01:00:16.371338', '102', 'Iztapa', 3, '', 7, 1),
(623, '2018-07-01 01:00:16.436333', '101', 'Puerto de San José', 3, '', 7, 1),
(624, '2018-07-01 01:00:16.471331', '100', 'Guanagazapa', 3, '', 7, 1),
(625, '2018-07-01 01:00:16.510330', '99', 'La Gomera', 3, '', 7, 1),
(626, '2018-07-01 01:00:16.559326', '98', 'Pueblo Nuevo Tiquisate', 3, '', 7, 1),
(627, '2018-07-01 01:00:16.598322', '97', 'Masagua', 3, '', 7, 1),
(628, '2018-07-01 01:00:16.626320', '96', 'Siquinalá', 3, '', 7, 1),
(629, '2018-07-01 01:00:16.708316', '95', 'La Democracia', 3, '', 7, 1),
(630, '2018-07-01 01:00:16.829309', '94', 'Santa Lucía Cotzumalguapa', 3, '', 7, 1),
(631, '2018-07-01 01:00:16.930819', '93', 'Escuintla', 3, '', 7, 1),
(632, '2018-07-01 01:00:17.177802', '92', 'Pachalúm', 3, '', 7, 1),
(633, '2018-07-01 01:00:17.229798', '91', 'Playa Grnade - Ixcán', 3, '', 7, 1),
(634, '2018-07-01 01:00:17.541299', '90', 'Chicaman', 3, '', 7, 1),
(635, '2018-07-01 01:00:17.602293', '89', 'Canilla', 3, '', 7, 1),
(636, '2018-07-01 01:00:17.640291', '88', 'San Bartolomé Jocotenango', 3, '', 7, 1),
(637, '2018-07-01 01:00:17.680287', '87', 'Sacapulas', 3, '', 7, 1),
(638, '2018-07-01 01:00:17.729285', '86', 'San Miguel Uspatán', 3, '', 7, 1),
(639, '2018-07-01 01:00:17.758282', '85', 'San Andrés Sajcabajá', 3, '', 7, 1),
(640, '2018-07-01 01:00:17.795278', '84', 'Santa María Nebaj', 3, '', 7, 1),
(641, '2018-07-01 01:00:17.824277', '83', 'Joyabaj', 3, '', 7, 1),
(642, '2018-07-01 01:00:17.862277', '82', 'San Juan Cotzal', 3, '', 7, 1),
(643, '2018-07-01 01:00:17.891274', '81', 'Cunén', 3, '', 7, 1),
(644, '2018-07-01 01:00:17.984266', '80', 'San Pedro Jocopilas', 3, '', 7, 1),
(645, '2018-07-01 01:00:18.040264', '79', 'San Antonio Ilotenango', 3, '', 7, 1),
(646, '2018-07-01 01:00:18.068263', '78', 'Patzité', 3, '', 7, 1),
(647, '2018-07-01 01:00:18.106259', '77', 'Santo Tomás Chichicstenango', 3, '', 7, 1),
(648, '2018-07-01 01:00:18.135257', '76', 'Chajul', 3, '', 7, 1),
(649, '2018-07-01 01:00:18.181255', '75', 'Zacualpa', 3, '', 7, 1),
(650, '2018-07-01 01:00:18.213254', '74', 'Chinique', 3, '', 7, 1),
(651, '2018-07-01 01:00:18.251250', '73', 'Chiche', 3, '', 7, 1),
(652, '2018-07-01 01:00:18.279247', '72', 'Santa Cruz del Quiche', 3, '', 7, 1),
(653, '2018-07-01 01:00:18.318247', '71', 'San Antonio La Paz', 3, '', 7, 1),
(654, '2018-07-01 01:00:18.346244', '70', 'Sanarate', 3, '', 7, 1),
(655, '2018-07-01 01:00:18.373242', '69', 'Sansare', 3, '', 7, 1),
(656, '2018-07-01 01:00:18.413240', '68', 'El Jícaro', 3, '', 7, 1),
(657, '2018-07-01 01:00:18.583228', '67', 'San Cristóbal Acasaguastlan', 3, '', 7, 1),
(658, '2018-07-01 01:00:18.636225', '66', 'San Agustín Acasaguastlan', 3, '', 7, 1),
(659, '2018-07-01 01:00:18.733219', '65', 'Morazán', 3, '', 7, 1),
(660, '2018-07-01 01:00:18.904313', '64', 'Guastatoya', 3, '', 7, 1),
(661, '2018-07-01 01:00:19.001304', '63', 'Poptún', 3, '', 7, 1),
(662, '2018-07-01 01:00:19.050301', '62', 'Melchor de Mencos', 3, '', 7, 1),
(663, '2018-07-01 01:00:19.100298', '61', 'Sayaxche', 3, '', 7, 1),
(664, '2018-07-01 01:00:19.138295', '60', 'San Luis', 3, '', 7, 1),
(665, '2018-07-01 01:00:19.168293', '59', 'Dolores', 3, '', 7, 1),
(666, '2018-07-01 01:00:19.193291', '58', 'Santa Ana', 3, '', 7, 1),
(667, '2018-07-01 01:00:19.234291', '57', 'San Francisco', 3, '', 7, 1),
(668, '2018-07-01 01:00:19.271286', '56', 'La Libertad', 3, '', 7, 1),
(669, '2018-07-01 01:00:19.340285', '55', 'San Andrés', 3, '', 7, 1),
(670, '2018-07-01 01:00:19.398282', '54', 'San Benito', 3, '', 7, 1),
(671, '2018-07-01 01:00:19.444276', '53', 'San José', 3, '', 7, 1),
(672, '2018-07-01 01:00:19.584267', '52', 'Flores', 3, '', 7, 1),
(673, '2018-07-01 01:00:19.622264', '51', 'Ipala', 3, '', 7, 1),
(674, '2018-07-01 01:00:19.659260', '50', 'San Jacinto', 3, '', 7, 1),
(675, '2018-07-01 01:00:19.688259', '49', 'Quezaltepeque', 3, '', 7, 1),
(676, '2018-07-01 01:00:19.726256', '48', 'Concepción Las Minas', 3, '', 7, 1),
(677, '2018-07-01 01:00:19.770254', '47', 'Esquipulas', 3, '', 7, 1),
(678, '2018-07-01 01:00:19.818251', '46', 'Olopa', 3, '', 7, 1),
(679, '2018-07-01 01:00:19.868247', '45', 'Camotán', 3, '', 7, 1),
(680, '2018-07-01 01:00:19.916244', '44', 'Jocotán', 3, '', 7, 1),
(681, '2018-07-01 01:00:19.971241', '43', 'San Juan Hermita', 3, '', 7, 1),
(682, '2018-07-01 01:00:20.031760', '42', 'San José La Arada', 3, '', 7, 1),
(683, '2018-07-01 01:00:20.092756', '41', 'Chiquimula', 3, '', 7, 1),
(684, '2018-07-01 01:00:20.227264', '40', 'El Tejar', 3, '', 7, 1),
(685, '2018-07-01 01:00:20.290260', '39', 'Zaragoza', 3, '', 7, 1),
(686, '2018-07-01 01:00:20.436250', '38', 'Parramos', 3, '', 7, 1),
(687, '2018-07-01 01:00:20.501248', '37', 'San Andrés Itzapa', 3, '', 7, 1),
(688, '2018-07-01 01:00:20.554243', '36', 'San Pedro Yepocapa', 3, '', 7, 1),
(689, '2018-07-01 01:00:20.682235', '35', 'Acatenango', 3, '', 7, 1),
(690, '2018-07-01 01:00:20.721231', '34', 'Santa Cruz Balanyá', 3, '', 7, 1),
(691, '2018-07-01 01:00:20.758229', '33', 'Patzicia', 3, '', 7, 1),
(692, '2018-07-01 01:00:20.787227', '32', 'San Miguel Pochuta', 3, '', 7, 1),
(693, '2018-07-01 01:00:49.923125', '31', 'Patzun', 3, '', 7, 1),
(694, '2018-07-01 01:00:50.118127', '30', 'Tecpán Guatemala', 3, '', 7, 1),
(695, '2018-07-01 01:00:50.146124', '29', 'Santa Apolonia', 3, '', 7, 1),
(696, '2018-07-01 01:00:50.185124', '28', 'San Juan Comalapa', 3, '', 7, 1),
(697, '2018-07-01 01:00:50.214121', '27', 'San Martín Jilotepeque', 3, '', 7, 1),
(698, '2018-07-01 01:00:50.261115', '26', 'San José Poaquil', 3, '', 7, 1),
(699, '2018-07-01 01:00:50.301113', '25', 'Chimaltenango', 3, '', 7, 1),
(700, '2018-07-01 01:00:50.339111', '24', 'Purulhá', 3, '', 7, 1),
(701, '2018-07-01 01:00:50.368109', '23', 'San Jerónimo', 3, '', 7, 1),
(702, '2018-07-02 14:49:15.380833', '1', 'Masculino', 1, '[{\"added\": {}}]', 13, 1),
(703, '2018-07-02 14:49:23.902839', '2', 'Femenino', 1, '[{\"added\": {}}]', 13, 1),
(704, '2018-07-02 14:49:37.334603', '2', 'Femenino', 3, '', 13, 1),
(705, '2018-07-02 14:49:37.525085', '1', 'Masculino', 3, '', 13, 1),
(706, '2018-07-02 14:49:46.912058', '3', 'Femenino', 1, '[{\"added\": {}}]', 13, 1),
(707, '2018-07-02 14:49:53.807422', '4', 'Masculino', 1, '[{\"added\": {}}]', 13, 1),
(708, '2018-07-02 14:51:05.886645', '1', 'Miguel Angel', 1, '[{\"added\": {}}]', 11, 1),
(709, '2018-07-02 14:51:15.854155', '1', 'Novela', 1, '[{\"added\": {}}]', 12, 1),
(710, '2018-07-02 14:51:24.186354', '1', 'El Señor Presidente', 1, '[{\"added\": {}}]', 10, 1),
(711, '2018-07-02 15:08:34.079288', '1', 'Miguel Angel Asturias', 3, '', 11, 1),
(712, '2018-07-02 15:15:38.033815', '2', 'Miguel Angel Asturias', 1, '[{\"added\": {}}]', 11, 1),
(713, '2018-07-02 15:15:50.170134', '2', 'Novela', 1, '[{\"added\": {}}]', 12, 1),
(714, '2018-07-02 15:15:57.977878', '2', 'El Señor Presidente', 1, '[{\"added\": {}}]', 10, 1),
(715, '2018-07-02 22:30:11.915429', '3', 'Daniel Guarchaj', 1, '[{\"added\": {}}]', 11, 1),
(716, '2018-07-02 22:30:11.917433', '4', 'Daniel Guarchaj', 1, '[{\"added\": {}}]', 11, 1),
(717, '2018-07-02 22:30:22.960940', '3', 'Otro', 1, '[{\"added\": {}}]', 12, 1),
(718, '2018-07-02 22:30:39.021059', '3', 'Algun Libro', 1, '[{\"added\": {}}]', 10, 1),
(719, '2018-07-04 17:19:00.161717', '1', 'Novela', 3, '', 12, 1),
(720, '2018-07-04 17:20:01.837725', '4', 'Deportes', 1, '[{\"added\": {}}]', 12, 1),
(721, '2018-07-04 17:20:20.628853', '5', 'Economia', 1, '[{\"added\": {}}]', 12, 1),
(722, '2018-07-04 17:39:03.637351', '1', 'capuche777', 2, '[{\"changed\": {\"fields\": [\"first_name\", \"last_name\"]}}]', 4, 1),
(723, '2018-07-04 17:48:08.850767', '2', 'Miguel Angel Asturias', 2, '[{\"changed\": {\"fields\": [\"gender\"]}}]', 11, 1),
(724, '2018-07-04 17:48:19.748718', '4', 'Daniel Guarchaj', 3, '', 11, 1),
(725, '2018-07-04 17:48:33.306648', '3', 'Daniel Guarchaj', 3, '', 11, 1),
(726, '2018-07-04 17:52:32.715813', '5', 'J. K. Rowling', 1, '[{\"added\": {}}]', 11, 1),
(727, '2018-07-04 17:53:16.707108', '4', 'Harry Potter', 1, '[{\"added\": {}}]', 10, 1),
(728, '2018-07-04 18:05:49.983315', '4', 'Harry Potter', 2, '[{\"changed\": {\"fields\": [\"availability\"]}}]', 10, 1),
(729, '2018-07-04 22:01:55.169749', '5', 'El futbolista frustrado', 1, '[{\"added\": {}}]', 10, 1),
(730, '2018-07-12 17:40:35.245101', '1', 'Prestado', 1, '[{\"added\": {}}]', 15, 1),
(731, '2018-07-12 17:40:42.469097', '2', 'Mora', 1, '[{\"added\": {}}]', 15, 1),
(732, '2018-07-12 17:40:51.232618', '3', 'Devuelto', 1, '[{\"added\": {}}]', 15, 1),
(733, '2018-07-12 17:40:58.853093', '4', 'Devuelto con mora', 1, '[{\"added\": {}}]', 15, 1),
(734, '2018-07-12 17:42:15.692369', '1', 'CKjaTfjGjef436EV', 1, '[{\"added\": {}}]', 16, 1),
(735, '2018-07-12 18:11:52.187569', '2', 'Gd9TQ6LrD82tEbjs', 1, '[{\"added\": {}}]', 16, 1),
(736, '2018-07-12 18:13:38.872623', '3', 'FkEUkBfPB9gQJbjC', 1, '[{\"added\": {}}]', 16, 1),
(737, '2018-07-12 18:13:55.907584', '3', 'FkEUkBfPB9gQJbjC', 2, '[{\"changed\": {\"fields\": [\"state\"]}}]', 16, 1),
(738, '2018-07-12 18:16:13.218342', '2', 'Gd9TQ6LrD82tEbjs', 2, '[{\"changed\": {\"fields\": [\"state\"]}}]', 16, 1),
(739, '2018-07-12 21:31:49.168477', '4', 'nm]HE]n5eS3dKXw1', 1, '[{\"added\": {}}]', 16, 1),
(740, '2018-07-12 21:54:42.450869', '2', 'cloarca', 1, '[{\"added\": {}}]', 4, 1),
(741, '2018-07-12 21:55:08.508852', '2', 'cloarca', 2, '[{\"changed\": {\"fields\": [\"first_name\", \"last_name\", \"email\", \"is_superuser\"]}}]', 4, 1),
(742, '2018-07-12 21:55:26.510523', '2', 'cloarca', 2, '[{\"changed\": {\"fields\": [\"is_staff\"]}}]', 4, 1),
(743, '2018-07-12 21:55:54.685037', '2', 'cloarca', 2, '[]', 4, 1),
(744, '2018-07-12 21:57:01.738938', '6', 'La calle', 1, '[{\"added\": {}}]', 10, 2),
(745, '2018-07-12 21:57:24.014486', '6', 'La calle', 2, '[{\"changed\": {\"fields\": [\"availability\"]}}]', 10, 2),
(746, '2018-07-12 21:57:46.969936', '5', 'El futbolista frustrado', 3, '', 10, 2),
(747, '2018-07-12 21:59:09.933977', '6', 'Juan Perez', 1, '[{\"added\": {}}]', 11, 2),
(748, '2018-07-12 21:59:37.703569', '6', 'Juan Gomez', 2, '[{\"changed\": {\"fields\": [\"surname\"]}}]', 11, 2),
(749, '2018-07-12 21:59:55.738380', '6', 'Juan Gomez', 3, '', 11, 2),
(750, '2018-07-12 22:00:15.542494', '6', 'Ocio', 1, '[{\"added\": {}}]', 12, 2),
(751, '2018-07-12 22:00:27.379204', '6', 'Ocio 2', 2, '[{\"changed\": {\"fields\": [\"name\"]}}]', 12, 2),
(752, '2018-07-12 22:00:38.779403', '6', 'Ocio 2', 3, '', 12, 2);
INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(753, '2018-07-12 22:08:41.070960', '1', 'CKjaTfjGjef436EV', 2, '[{\"changed\": {\"fields\": [\"state\"]}}]', 16, 2),
(754, '2018-07-12 22:08:50.953975', '4', 'nm]HE]n5eS3dKXw1', 2, '[{\"changed\": {\"fields\": [\"state\"]}}]', 16, 2),
(755, '2018-07-12 22:09:14.037314', '5', 'MpSPW1HKXWyK6F8e', 1, '[{\"added\": {}}]', 16, 2),
(756, '2018-07-16 21:07:03.033726', '1', 'capuche777', 2, '[{\"changed\": {\"fields\": [\"password\"]}}]', 4, 2),
(757, '2018-07-19 16:04:56.246828', '1', 'capuche777', 2, '[{\"changed\": {\"fields\": [\"password\"]}}]', 4, 3),
(758, '2018-07-19 16:54:07.489333', '4', 'otronumerouno', 1, '[{\"added\": {}}]', 4, 1),
(759, '2018-07-19 16:55:04.816823', '4', 'otronumerouno', 2, '[{\"changed\": {\"fields\": [\"first_name\", \"last_name\", \"email\", \"is_staff\", \"is_superuser\"]}}]', 4, 1),
(760, '2018-07-19 16:55:38.174753', '4', 'otronumerouno', 2, '[{\"changed\": {\"fields\": [\"password\"]}}]', 4, 1),
(761, '2018-07-19 16:57:59.085633', '6', 'Gustavo Seijas', 1, '[{\"added\": {}}]', 11, 4),
(762, '2018-07-19 16:58:21.155958', '6', 'Gustavo Seijas', 2, '[{\"changed\": {\"fields\": [\"gender\", \"death\"]}}]', 11, 4),
(763, '2018-07-19 16:58:58.377764', '6', 'Escolar', 1, '[{\"added\": {}}]', 12, 4),
(764, '2018-07-19 16:59:47.250289', '7', 'Diseño sobre la optimización de los envases plasticos', 1, '[{\"added\": {}}]', 10, 4),
(765, '2018-07-19 17:00:56.119266', '6', 'h2wdkbfSSj6jfLAt', 1, '[{\"added\": {}}]', 16, 4),
(766, '2018-07-19 17:01:15.262613', '6', 'h2wdkbfSSj6jfLAt', 2, '[{\"changed\": {\"fields\": [\"state\"]}}]', 16, 4),
(767, '2018-07-19 17:01:41.069351', '6', 'h2wdkbfSSj6jfLAt', 2, '[{\"changed\": {\"fields\": [\"state\"]}}]', 16, 4),
(768, '2018-07-19 17:02:52.636343', '8', 'noticia nueva', 1, '[{\"added\": {}}]', 10, 4),
(769, '2018-07-21 05:46:56.946354', '2', 'cloarca', 2, '[{\"changed\": {\"fields\": [\"is_staff\", \"is_superuser\"]}}]', 4, 1),
(770, '2018-07-21 05:47:25.300948', '2', 'cloarca', 2, '[{\"changed\": {\"fields\": [\"password\"]}}]', 4, 1),
(771, '2018-07-24 15:36:00.125082', '2', 'cloarca', 2, '[{\"changed\": {\"fields\": [\"password\"]}}]', 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(11, 'books', 'author'),
(13, 'books', 'genre'),
(12, 'books', 'theme'),
(10, 'books', 'title'),
(5, 'contenttypes', 'contenttype'),
(14, 'core', 'genre'),
(15, 'core', 'lend_state'),
(9, 'country', 'country'),
(7, 'country', 'department'),
(8, 'country', 'municipality'),
(16, 'lends', 'lend'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2018-06-30 01:06:19.965433'),
(2, 'auth', '0001_initial', '2018-06-30 01:06:34.038143'),
(3, 'admin', '0001_initial', '2018-06-30 01:06:36.579389'),
(4, 'admin', '0002_logentry_remove_auto_add', '2018-06-30 01:06:36.642362'),
(5, 'contenttypes', '0002_remove_content_type_name', '2018-06-30 01:06:38.174472'),
(6, 'auth', '0002_alter_permission_name_max_length', '2018-06-30 01:06:38.450921'),
(7, 'auth', '0003_alter_user_email_max_length', '2018-06-30 01:06:38.763618'),
(8, 'auth', '0004_alter_user_username_opts', '2018-06-30 01:06:38.824448'),
(9, 'auth', '0005_alter_user_last_login_null', '2018-06-30 01:06:39.938873'),
(10, 'auth', '0006_require_contenttypes_0002', '2018-06-30 01:06:39.984940'),
(11, 'auth', '0007_alter_validators_add_error_messages', '2018-06-30 01:06:40.088553'),
(12, 'auth', '0008_alter_user_username_max_length', '2018-06-30 01:06:40.844500'),
(13, 'auth', '0009_alter_user_last_name_max_length', '2018-06-30 01:06:41.137623'),
(14, 'sessions', '0001_initial', '2018-06-30 01:06:42.338702'),
(15, 'country', '0001_initial', '2018-06-30 21:33:48.267272'),
(16, 'books', '0001_initial', '2018-07-02 14:45:13.267997'),
(17, 'books', '0002_auto_20180702_0911', '2018-07-02 15:11:32.068015'),
(18, 'books', '0003_auto_20180704_1141', '2018-07-04 17:41:29.967064'),
(19, 'core', '0001_initial', '2018-07-04 17:41:30.278196'),
(20, 'books', '0004_auto_20180704_1147', '2018-07-04 17:47:38.906213'),
(21, 'books', '0005_auto_20180711_1128', '2018-07-12 17:33:52.247960'),
(22, 'core', '0002_lend_state', '2018-07-12 17:33:52.633758'),
(23, 'core', '0003_auto_20180711_1132', '2018-07-12 17:33:54.227778'),
(24, 'lends', '0001_initial', '2018-07-12 17:34:02.250104'),
(25, 'lends', '0002_auto_20180712_1139', '2018-07-12 17:39:20.405562'),
(26, 'lends', '0003_auto_20180712_1529', '2018-07-12 21:29:53.092639');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('1t2r8e4zicqo3xqzzwvpp6k88yjd4ip5', 'NzdkODIwZWViOTFlNGQ0NjY3YzFmNTQ0NjQ0MTUxZjZlYmQ0MzJiYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyODM1YTg5NmJiN2QxNTFiMzFkNmI1ZTdmNjc4M2ZhMzg5ODVjNDU0In0=', '2018-07-14 21:34:08.199893'),
('2bsfmy3pxp5iaiylluq1a3836n1aaqf3', 'NmM0OTNmNWQxNTgzMjE5N2ExMzM2YTcyMjcxZGUwNmU4NDUxZDk0Zjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI1MDVjN2VlMjJjNjZiNmUxY2Y0NjIzNTM2MWY0OTUxNDM3NjE3ZTY1In0=', '2018-07-30 21:08:20.043967'),
('5var0uy4p90znp26k4hh1nehlygo84y1', 'NzdkODIwZWViOTFlNGQ0NjY3YzFmNTQ0NjQ0MTUxZjZlYmQ0MzJiYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyODM1YTg5NmJiN2QxNTFiMzFkNmI1ZTdmNjc4M2ZhMzg5ODVjNDU0In0=', '2018-07-18 17:18:27.413070'),
('aqpu18d7p22xmfc52iodsewr3b97x2r7', 'YmViNmNkMzU3ODcyY2RiMjM5NGU5ZDdhZTg5Y2UwYTE3NGZjZDYwYjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4OGJhMjc2NGJjNWViNWVhZjRlN2JkZTUxNGE3YzExZDlmNGE4Yjc2In0=', '2018-08-07 20:07:39.341907'),
('bbwisxtsstu9kowmh1biounmkh8rytre', 'NzdkODIwZWViOTFlNGQ0NjY3YzFmNTQ0NjQ0MTUxZjZlYmQ0MzJiYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyODM1YTg5NmJiN2QxNTFiMzFkNmI1ZTdmNjc4M2ZhMzg5ODVjNDU0In0=', '2018-07-16 14:46:48.575564'),
('lwtjqn9olxmaj9zhl9mb1dislu3tpums', 'ODgxOTc5M2I5NDMxMTYyZThmMjAyNjZjODk5ZTU2ODg0NTg5OGNkOTp7Il9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJiZDAxYmI2MDdhODk2N2I0ZjE0MjdkYzE5MzE4ODgzNmJiOWFiOWFkIn0=', '2018-08-02 16:56:17.441107'),
('mcmmjrzzsshp5hb51l69ppixth6dwe6r', 'YmViNmNkMzU3ODcyY2RiMjM5NGU5ZDdhZTg5Y2UwYTE3NGZjZDYwYjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4OGJhMjc2NGJjNWViNWVhZjRlN2JkZTUxNGE3YzExZDlmNGE4Yjc2In0=', '2018-08-04 05:48:07.928258'),
('ugm6rpokldf52kolfw5xzlp5bm6crg69', 'NzdkODIwZWViOTFlNGQ0NjY3YzFmNTQ0NjQ0MTUxZjZlYmQ0MzJiYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIyODM1YTg5NmJiN2QxNTFiMzFkNmI1ZTdmNjc4M2ZhMzg5ODVjNDU0In0=', '2018-07-16 22:29:02.771624'),
('ugvust4gtwn7vm3z7g2kmlds9w32uwkn', 'MTAwM2Y1NmVjYTYxYzRmZDk3ODYxNjQ1ODE2MDVlNDc1NzRiMDQ5MTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxZTJkNGY0YzgyZjdiOWU5YjkwMzRiMjg3OGI2NDUyNGNlNjBiNDAwIn0=', '2018-08-07 22:00:47.744533');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lends_lend`
--

CREATE TABLE `lends_lend` (
  `id` int(11) NOT NULL,
  `return_date` date NOT NULL,
  `token` varchar(16) NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `book_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `lends_lend`
--

INSERT INTO `lends_lend` (`id`, `return_date`, `token`, `created`, `updated`, `book_id`, `state_id`, `user_id`) VALUES
(3, '2018-07-20', 'FkEUkBfPB9gQJbjC', '2018-07-12 18:13:38.859727', '2018-07-12 18:13:55.809842', 4, 3, 1),
(6, '2018-07-27', 'h2wdkbfSSj6jfLAt', '2018-07-19 17:00:56.004267', '2018-07-19 17:01:41.049833', 7, 4, 4),
(7, '2018-08-01', 'fuxrpccrYE7Lv7ng', '2018-07-24 20:08:30.326870', '2018-07-24 20:08:30.326870', 7, 1, 2),
(8, '2018-08-01', 'a9MN7pbzBPSamjDj', '2018-07-24 20:12:26.749839', '2018-07-24 20:12:26.750820', 8, 1, 2),
(9, '2018-08-12', '6YkzrHyQxCUDJTje', '2018-07-24 22:06:51.233422', '2018-07-24 22:06:51.233422', 4, 1, 2),
(10, '2018-07-25', '6YkzrHyQxCUDJTje', '2018-07-24 22:12:30.141431', '2018-07-24 22:12:30.141431', 4, 1, 2),
(11, '2018-07-25', '4P4gzRP5M2xpd7BP', '2018-07-24 22:13:49.134203', '2018-07-24 22:13:49.134203', 7, 1, 2),
(12, '2018-07-25', '4P4gzRP5M2xpd7BP', '2018-07-24 22:14:17.708847', '2018-07-24 22:14:17.708847', 8, 1, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indices de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indices de la tabla `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indices de la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indices de la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indices de la tabla `books_author`
--
ALTER TABLE `books_author`
  ADD PRIMARY KEY (`id`),
  ADD KEY `books_author_country_id_aaa9aad6_fk_country_country_id` (`country_id`),
  ADD KEY `books_author_gender_id_920a68ad_fk_core_genre_id` (`gender_id`);

--
-- Indices de la tabla `books_theme`
--
ALTER TABLE `books_theme`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `books_theme_name_f0e677b6_uniq` (`name`);

--
-- Indices de la tabla `books_title`
--
ALTER TABLE `books_title`
  ADD PRIMARY KEY (`id`),
  ADD KEY `books_title_author_id_c069412e_fk_books_author_id` (`author_id`),
  ADD KEY `books_title_topic_id_1db4237e_fk_books_theme_id` (`topic_id`);

--
-- Indices de la tabla `core_genre`
--
ALTER TABLE `core_genre`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `genre` (`genre`);

--
-- Indices de la tabla `core_lend_state`
--
ALTER TABLE `core_lend_state`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `state` (`state`);

--
-- Indices de la tabla `country_country`
--
ALTER TABLE `country_country`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `country_department`
--
ALTER TABLE `country_department`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_department_country_id_f7d35672_fk_country_country_id` (`country_id`);

--
-- Indices de la tabla `country_municipality`
--
ALTER TABLE `country_municipality`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_municipality_department_id_b8e4e842_fk_country_d` (`department_id`);

--
-- Indices de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indices de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indices de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indices de la tabla `lends_lend`
--
ALTER TABLE `lends_lend`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lends_lend_book_id_22bf0a8e_fk_books_title_id` (`book_id`),
  ADD KEY `lends_lend_state_id_052364f7_fk_core_lend_state_id` (`state_id`),
  ADD KEY `lends_lend_user_id_f22be94b_fk_auth_user_id` (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `books_author`
--
ALTER TABLE `books_author`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `books_theme`
--
ALTER TABLE `books_theme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `books_title`
--
ALTER TABLE `books_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `core_genre`
--
ALTER TABLE `core_genre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `core_lend_state`
--
ALTER TABLE `core_lend_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `country_country`
--
ALTER TABLE `country_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;

--
-- AUTO_INCREMENT de la tabla `country_department`
--
ALTER TABLE `country_department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `country_municipality`
--
ALTER TABLE `country_municipality`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=332;

--
-- AUTO_INCREMENT de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=772;

--
-- AUTO_INCREMENT de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `lends_lend`
--
ALTER TABLE `lends_lend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Filtros para la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Filtros para la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `books_author`
--
ALTER TABLE `books_author`
  ADD CONSTRAINT `books_author_country_id_aaa9aad6_fk_country_country_id` FOREIGN KEY (`country_id`) REFERENCES `country_country` (`id`),
  ADD CONSTRAINT `books_author_gender_id_920a68ad_fk_core_genre_id` FOREIGN KEY (`gender_id`) REFERENCES `core_genre` (`id`);

--
-- Filtros para la tabla `books_title`
--
ALTER TABLE `books_title`
  ADD CONSTRAINT `books_title_author_id_c069412e_fk_books_author_id` FOREIGN KEY (`author_id`) REFERENCES `books_author` (`id`),
  ADD CONSTRAINT `books_title_topic_id_1db4237e_fk_books_theme_id` FOREIGN KEY (`topic_id`) REFERENCES `books_theme` (`id`);

--
-- Filtros para la tabla `country_department`
--
ALTER TABLE `country_department`
  ADD CONSTRAINT `country_department_country_id_f7d35672_fk_country_country_id` FOREIGN KEY (`country_id`) REFERENCES `country_country` (`id`);

--
-- Filtros para la tabla `country_municipality`
--
ALTER TABLE `country_municipality`
  ADD CONSTRAINT `country_municipality_department_id_b8e4e842_fk_country_d` FOREIGN KEY (`department_id`) REFERENCES `country_department` (`id`);

--
-- Filtros para la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `lends_lend`
--
ALTER TABLE `lends_lend`
  ADD CONSTRAINT `lends_lend_book_id_22bf0a8e_fk_books_title_id` FOREIGN KEY (`book_id`) REFERENCES `books_title` (`id`),
  ADD CONSTRAINT `lends_lend_state_id_052364f7_fk_core_lend_state_id` FOREIGN KEY (`state_id`) REFERENCES `core_lend_state` (`id`),
  ADD CONSTRAINT `lends_lend_user_id_f22be94b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
