from django.contrib import admin
from .models import Lend


# Register your models here.
class LendAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated')
    list_display = ('token', 'book', 'get_author', 'get_theme', 'created', 'return_date', 'user', 'state')
    search_fields = ('token', 'book__title', 'user__username', 'state__state')
    date_hierarchy = 'created'
    ordering = ('return_date', '-state', 'token')

    def get_author(self, obj):
        return obj.book.author
    get_author.short_description = 'Autor'
    get_author.admin_order_field = 'book__title'

    def get_theme(self, obj):
        return obj.book.topic
    get_theme.short_description = 'Tema'


admin.site.register(Lend, LendAdmin)