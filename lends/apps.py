from django.apps import AppConfig


class LendsConfig(AppConfig):
    name = 'lends'
    verbose_name = 'Gestor de Prestamos'