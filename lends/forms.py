from django import forms
from .models import Lend


class LendForm(forms.ModelForm):
    class Meta:
        model = Lend
        fields = ['user', 'book', 'return_date', 'state']