from django.urls import path
from .views import LendList, LendCreate, LendUpdate, LendDelete, LendCreateUser

lend_patterns = ([
    path('', LendList.as_view(), name='prestamos'),
    path('create/', LendCreate.as_view(), name='crear-prestamo'),
    path('user/create/', LendCreateUser.as_view(), name='usuario-crear-prestamo'),
    path('update/<int:pk>', LendUpdate.as_view(), name='actualizar-prestamo'),
    path('delete/<int:pk>', LendDelete.as_view(), name='eliminar-prestamo'),
], 'lends')