from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.urls import reverse, reverse_lazy
from django.shortcuts import redirect
from .models import Lend
from .forms import LendForm


# Create your views here.
@method_decorator(staff_member_required, name='dispatch')
class LendCreate(CreateView):
    model = Lend
    form_class = LendForm
    success_url = reverse_lazy('books:libros')


# Create your views here.
@method_decorator(login_required, name='dispatch')
class LendCreateUser(CreateView):
    model = Lend
    form_class = LendForm
    success_url = reverse_lazy('lends:prestamos')


@method_decorator(login_required, name='dispatch')
class LendList(ListView):
    model = Lend


@method_decorator(staff_member_required, name='dispatch')
class LendUpdate(UpdateView):
    model = Lend
    form_class = LendForm
    template_name_suffix = '_update_form'

    def get_success_url(self):
        return reverse_lazy('lends:actualizar-prestamo', args=[self.object.id]) + '?ok'


@method_decorator(staff_member_required, name='dispatch')
class LendDelete(DeleteView):
    model = Lend
    success_url = reverse_lazy('lends:prestamos')
