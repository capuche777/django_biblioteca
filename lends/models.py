from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now, timedelta
from django.utils.crypto import get_random_string
from books.models import Title, Author, Theme
from core.models import Lend_State


# Create your models here.


class Lend(models.Model):
    user = models.ForeignKey(User, verbose_name='Usuario', on_delete=models.CASCADE)
    book = models.ForeignKey(Title, verbose_name='Libro', on_delete=models.CASCADE)
    return_date = models.DateField(verbose_name='Fecha de devolución', default=now()+timedelta(days=8))
    token = models.CharField(max_length=16, verbose_name='Token', default=get_random_string(length=16, allowed_chars='abcdefghj]kmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ123456789'))
    state = models.ForeignKey(Lend_State, verbose_name='Estado', on_delete=models.CASCADE, default=1)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de Prestamo')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de edición')


    class Meta:
        verbose_name = 'prestamo'
        verbose_name_plural = 'prestamos'
        ordering = ['-created']


    def __str__(self):
        return self.token